package org.tanks;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import org.tanks.Assets.Assets;
import org.tanks.GameStates.*;
import org.tanks.Input.KeyManager;
import org.tanks.Input.MouseManager;

/**
 * The {@code World} class is responsible for initializing the window, input managers, world thread, assets
 * and the game settings.
 * <p>
 * When its start method is called it starts the world thread - the thread that runs the main game loop.
 *
 * @author Beldiman Vladislav
 */
public class World implements Runnable {
    /**
     * color of the window's background
     */
    public static final Color BACKGROUND_COLOR = new Color(75, 83, 32);

    /**
     * current game state
     */
    private GameState gameState;

    /**
     * flag that is true if the world thread is running
     */
    private boolean running;

    /**
     * the thread that executes the main game loop
     */
    private final Thread worldThread;

    /**
     * the key manager of the game
     */
    private final KeyManager keyManager;

    /**
     * the mouse manager of the game
     */
    private final MouseManager mouseManager;

    /**
     * the window of the game
     */
    private final GameWindow window;

    /**
     * the difficulty of the game
     */
    private int difficulty;

    /**
     * current cached highscore
     */
    private int highscore;

    /**
     * Sole constructor of the world. Responsible for constructing world thread, key and mouse managers,
     * window, initializing difficulty, highscore and sound from database, assets, and game state.
     *
     * @param title title of the window
     * @param width width of the window
     * @param height height of the window
     */
    public World(String title, int width, int height) {
        worldThread = new Thread(this, "worldThread");

        keyManager = new KeyManager();
        mouseManager = new MouseManager();

        window = new GameWindow(title, width, height);
        window.addKeyListener(keyManager);
        window.addMouseListener(mouseManager);

        try {
            Assets.initAssets();
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
            System.exit(100);
        }

        difficulty = Game.LOCATOR.getDatabaseService().getSetting("difficulty");
        highscore = Game.LOCATOR.getDatabaseService().getSetting("highscore");
        boolean soundOn = Game.LOCATOR.getDatabaseService().getSetting("soundMuted") == 0;

        if (soundOn) {
            Game.LOCATOR.turnOnAudio();
        }

        gameState = new StoryState();
    }

    /**
     * Executes the main game loop. Is ran by the world thread
     */
    public void run() {
        // update and frame display (all commented code) is only for debugging purpose
//        int updates = 0;
//        int frames = 0;
//        long timer = System.currentTimeMillis();

        // Sets target fps
        final int targetFPS = 60;

        // Calculate nanoseconds per frame
        final long timePerFrame = 1_000_000_000 / targetFPS;

        long previousIterationStart = System.nanoTime();
        long lag = 0;

        while (running) {
            long currentIterationStart = System.nanoTime();

            // real world elapsed time
            long elapsed = currentIterationStart - previousIterationStart;

            previousIterationStart = currentIterationStart;

            // real world time lag
            lag += elapsed;

            handleInput();

            // catch up to real world time
            // updates only if lag is greater than time per frame
            while (lag >= timePerFrame) {
//                updates++;
                update();
                lag -= timePerFrame;
            }

            draw();

//            frames++;
//            if (System.currentTimeMillis() - timer > 1000) {
//                 System.out.println("UPDATES " + updates + " FPS " + frames);
//                 timer += 1000;
//                 updates = 0;
//                 frames = 0;
//            }
        }
    }

    /**
     * Starts the world thread if not already running.
     */
    public synchronized void start() {
        if (running)
            return;

        worldThread.start();
        running = true;
    }

    /**
     * Stops the world thread if not already stopped.
     */
    public synchronized void stop() {
        if(!running)
            return;
        running = false;

        try {
            worldThread.join();
        }
        catch(InterruptedException ex) {
            ex.printStackTrace();
        }

        Game.LOCATOR.muteAudio();
        window.dispose();
    }

    /**
     * Sets the new game state if not null.
     *
     * @param nextState a new game state that is null if no change is necessary
     * @see GameState
     */
    private void setGameState(GameState nextState) {
        if (nextState != null) {
            gameState.exit(this);
            gameState = nextState;
            gameState.enter(this);
        }
    }

    /**
     * Delegates input handling to current game state and updates it if necessary.
     */
    private void handleInput() {
        GameState nextState = gameState.handleInput(this, keyManager.getInput(), mouseManager.getInput());
        setGameState(nextState);
    }

    /**
     * Delegates updating to current game state.
     */
    private void update() {
        gameState.update(this);
    }

    /**
     * Draws window background and delegates component drawing to current game state.
     */
    private void draw() {
        BufferStrategy bufferStrategy = window.getBufferStrategy();

        // Render frame
        do {
            do {
                Graphics graphics = bufferStrategy.getDrawGraphics();

                // Draw background
                graphics.setColor(BACKGROUND_COLOR);
                graphics.fillRect(0, 0, window.getWidth(), window.getHeight());

                gameState.draw(graphics);

                graphics.dispose();

                // Repeat the rendering if the drawing buffer contents
                // were restored
            } while (bufferStrategy.contentsRestored());

            bufferStrategy.show();

            // Repeat the rendering if the drawing buffer was lost
        } while (bufferStrategy.contentsLost());
    }

    /**
     * Obtains window width.
     *
     * @return window width
     */
    public int getWidth() {
        return window.getWidth();
    }

    /**
     * Obtains window height.
     *
     * @return window height
     */
    public int getHeight() {
        return window.getHeight();
    }

    /**
     * Obtains world bounds.
     *
     * @return a rectangle representing world bounds.
     */
    public Rectangle getBounds() { return new Rectangle(0, 0, getWidth(), getHeight()); }

    /**
     * Obtains game difficulty.
     *
     * @return game difficulty
     */
    public int getDifficulty() { return difficulty; }

    /**
     * Sets game difficulty.
     *
     * @param difficulty difficulty to set
     */
    public void setDifficulty(int difficulty) { this.difficulty = difficulty; }

    /**
     * Obtains current highscore.
     *
     * @return current highscore
     */
    public int getHighscore() { return highscore; }

    /**
     * Sets new highscore.
     *
     * @param highscore new highscore
     */
    public void setHighscore(int highscore) { this.highscore = highscore; }
}

