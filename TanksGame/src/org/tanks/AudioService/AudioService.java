package org.tanks.AudioService;

/**
 * The {@code AudioService} interface represents a service which provides control over in game sounds.
 *
 * @author Beldiman Vladislav
 */
public interface AudioService {
    /**
     * Plays a sound effect chosen through a sound id.
     *
     * @param soundID sound id of the sound effect
     * @see SoundID
     */
    void playSound(SoundID soundID);

    /**
     * Plays the music track, resetting it if already playing.
     */
    void playMusic();

    /**
     * Stops the music track if not already playing.
     */
    void stopMusic();
}
