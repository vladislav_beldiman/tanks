package org.tanks.AudioService;

import javax.sound.sampled.*;
import java.io.IOException;

/**
 * The {@code FullAudioService} class provides full in game sound capabilities.
 *
 * @see AudioService
 * @author Beldiman Vladislav
 */
public class FullAudioService implements AudioService {
    /**
     * the clip for the fire sound effect
     */
    private final Clip fire;

    /**
     * the clip for the blow up sound effect
     */
    private final Clip blowUp;

    /**
     * the clip for the level up sound effect
     */
    private final Clip levelUp;

    /**
     * the clip for the music track
     */
    private final Clip music;

    /**
     * Sole contructor. Tries to load all clips and throws on failure to do so.
     *
     * @throws IOException if an I/O exception occurs during reading of the stream
     * @throws LineUnavailableException if the line cannot be opened due to resource restrictions
     * @throws UnsupportedAudioFileException  if the URL does not point to valid audio file data
     *                                        recognized by the system
     */
    public FullAudioService() throws IOException, LineUnavailableException,
                                        UnsupportedAudioFileException {
        fire = loadClip("/sounds/fire.wav");
        controlVolume(fire, 0.25);

        blowUp = loadClip("/sounds/blowUp.wav");
        controlVolume(blowUp, 0.25);

        levelUp = loadClip("/sounds/levelUp.wav");
        controlVolume(levelUp, 0.25);

        music = loadClip("/sounds/music.wav");
        controlVolume(music, 0.1);
    }

    @Override
    public void playSound(SoundID soundID) {
        switch (soundID) {
            case FIRE -> playClip(fire);
            case BLOW_UP -> playClip(blowUp);
            case LEVEL_UP -> playClip(levelUp);
        }
    }

    @Override
    public void playMusic() {
        music.setFramePosition(0);
        music.loop(Clip.LOOP_CONTINUOUSLY);
    }

    @Override
    public void stopMusic() {
        music.stop();
    }

    /**
     * Loads a clip from an audio file.
     *
     * @param path path to the audio file
     * @return the clip read from the audio file
     * @throws IOException if an I/O exception occurs during reading of the stream
     * @throws LineUnavailableException if the line cannot be opened due to resource restrictions
     * @throws UnsupportedAudioFileException if the URL does not point to valid audio file data
     *                                       recognized by the system
     */
    private Clip loadClip(String path) throws IOException, LineUnavailableException,
                                                    UnsupportedAudioFileException {
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(getClass().getResource(path));
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);

        return clip;
    }

    /**
     * Lowers the volume of a clip.
     *
     * @param clip a clip whose volume is to be lowered
     * @param linearGain gain in units
     * @see FloatControl
     */
    private void controlVolume(Clip clip, double linearGain) {
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

        // calculates the gain in db
        float dBGain = (float) (Math.log(linearGain) / Math.log(10.0) * 20.0);
        gainControl.setValue(dBGain);
    }

    /**
     * Plays the sound effect. Resets it if already playing.
     *
     * @param clip a clip to be played
     */
    private void playClip(Clip clip) {
        if(clip.isRunning()) {
            clip.stop();
        }

        clip.setFramePosition(0);
        clip.start();
    }
}
