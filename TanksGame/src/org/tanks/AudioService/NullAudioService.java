package org.tanks.AudioService;

/**
 * The {@code NullAudioService} class provides a "do nothing" implementation for all audio service
 * methods.
 *
 * @see AudioService
 * @author Beldiman Vladislav
 */
public class NullAudioService implements AudioService {
    @Override
    public void playSound(SoundID soundID) {
        /* do nothing */
    }

    @Override
    public void playMusic() {
        /* do nothing */
    }

    @Override
    public void stopMusic() {
        /* do nothing */
    }
}
