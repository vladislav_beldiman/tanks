package org.tanks.AudioService;

/**
 * Sounds that can be played
 *
 * @author Beldiman Vladislav
 */
public enum SoundID {
    /**
     * Fire sound
     */
    FIRE,

    /**
     * Blow up sound
     */
    BLOW_UP,

    /**
     * Level up sound
     */
    LEVEL_UP
}
