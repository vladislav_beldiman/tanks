package org.tanks;

/**
 * The {@code Game} class is the top class of the project. It is responsible for creating the world
 * object and starting its thread.
 *
 * @author Beldiman Vladislav
 */
public class Game {
    /**
     * window width
     */
    public static final int WINDOW_WIDTH = 1280;

    /**
     * window width
     */
    public static final int WINDOW_HEIGHT = 780;

    public static final Locator LOCATOR = new Locator();

    public static void main(String[] args) {
        World world = new World("Tanks", WINDOW_WIDTH, WINDOW_HEIGHT);
        world.start();
    }
}
