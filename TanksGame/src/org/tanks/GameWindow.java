package org.tanks;

import org.tanks.Input.KeyManager;
import org.tanks.Input.MouseManager;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

/**
 * The {@code GameWindow} class composes functionality of both {@code JFrame} and {@code Canvas} classes.
 * It also has the key and mouse listeners attached.
 *
 * @author Beldiman Vladislav
 */
public class GameWindow {
    /**
     * jframe of the game window
     */
    private final JFrame FRAME;

    /**
     * canvas of the game window
     */
    private final Canvas CANVAS;

    /**
     * Sole constructor
     *
     * @param title title of the window
     * @param width width of the window
     * @param height height of the window
     */
    public GameWindow(String title, int width, int height){
        FRAME = new JFrame(title);
        initWindowFrame(width, height);
        CANVAS = new Canvas();
        initCanvas(width, height);
        FRAME.add(CANVAS);
        FRAME.pack();

        CANVAS.createBufferStrategy(3);
        CANVAS.requestFocus();
    }

    /**
     * Initializes jframe settings
     *
     * @param width width of the window
     * @param height height of the window
     */
    private void initWindowFrame(int width, int height) {
        FRAME.setPreferredSize(new Dimension(width, height));
        FRAME.setMaximumSize(new Dimension(width, height));
        FRAME.setMinimumSize(new Dimension(width, height));
        FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        FRAME.setResizable(false);
        FRAME.setFocusable(true);
        FRAME.setLocationRelativeTo(null);
        FRAME.setVisible(true);
    }

    /**
     * Initializes canvas settings
     *
     * @param width width of the window
     * @param height height of the window
     */
    private void initCanvas(int width, int height) {
        CANVAS.setPreferredSize(new Dimension(width, height));
        CANVAS.setMaximumSize(new Dimension(width, height));
        CANVAS.setMinimumSize(new Dimension(width, height));
    }

    /**
     * Adds a key listener to both the canvas and the jframe.
     *
     * @param keyManager key manager to add
     */
    public void addKeyListener(KeyManager keyManager) {
        CANVAS.addKeyListener(keyManager);
        FRAME.addKeyListener(keyManager);
    }

    /**
     * Adds a mouse listener to both the canvas and the jframe.
     *
     * @param mouseManager mouse manager to add
     */
    public void addMouseListener(MouseManager mouseManager) {
        CANVAS.addMouseListener(mouseManager);
        FRAME.addMouseListener(mouseManager);
    }

    /**
     * Obtains canvas buffer strategy.
     *
     * @return buffer strategy of the canvas
     */
    public BufferStrategy getBufferStrategy() { return CANVAS.getBufferStrategy(); }

    /**
     * Obtains the height of the window.
     *
     * @return height of the window
     */
    public int getHeight() { return FRAME.getHeight(); }

    /**
     * Obtains the width of the window.
     *
     * @return width of the window
     */
    public int getWidth() { return FRAME.getWidth(); }

    /**
     * Dispose of the window when the world thread is stopped
     */
    public void dispose() { FRAME.dispose(); }
}
