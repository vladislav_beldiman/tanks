package org.tanks.Entities;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Graphics.GraphicsComponent;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Input.KeyInput;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Entity} class is an abstract class used to create objects composable with graphics, input
 * and physics components.
 *
 * @see GraphicsComponent
 * @see org.tanks.Entities.Components.Input.InputComponent
 * @see org.tanks.Entities.Components.Physics.PhysicsComponent
 * @author or Beldiman Vladislav
 */
public abstract class Entity {
    /**
     * allegiane of the entity
     */
    private final Allegiance ALLEGIANCE;

    /**
     * buffered image of the texture
     */
    protected BufferedImage texture;

    /**
     * x coordinate
     */
    protected double x;

    /**
     * y coordinate
     */
    protected double y;

    /**
     * width of this
     */
    protected int width;

    /**
     * height of this
     */
    protected int height;

    /**
     * graphics component of this
     */
    private static final GraphicsComponent GRAPHICS_COMPONENT = new GraphicsComponent();

    /**
     * Sole constructor
     *
     * @param allegiance new allegiance
     * @param texture buffered image for texture
     * @param x new x coordinates
     * @param y new y coordinates
     * @param width new width
     * @param height new height
     * @see Allegiance
     */
    public Entity(Allegiance allegiance, BufferedImage texture, double x, double y, int width, int height) {
        this.ALLEGIANCE = allegiance;
        this.texture = texture;

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Handles input for this. Usually it is delegated to input component.
     *
     * @param input key input
     */
    public void handleInput(KeyInput input) { /* do nothing */ }

    /**
     * Updates this. Usually it is delegated to physics component.
     *
     * @param worldBounds a rectangle representing world boundaries
     * @param map a map of the world with which to check collision
     * @param obstacleManager an obstacle manager of the world with which to check collision
     * @param playerManager a player manager of the world with which to check collision
     * @param enemyPool an enemy pool of the world with which to check collision
     * @param projectileManager a projectile manager of the world with which to check collision
     * @param pickupsPool a pickups pool of the world with which to check collision
     * @param effectsPool an effects pool from which to play destroy animations
     * @param score a score object to add to
     * @see Entity
     * @see Map
     * @see ObstacleManager
     * @see PlayerManager
     * @see EnemyPool
     * @see ProjectileManager
     * @see PickupsPool
     * @see EffectsPool
     * @see Score
     */
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager,
                       PlayerManager playerManager, EnemyPool enemyPool,
                       ProjectileManager projectileManager, PickupsPool pickupsPool,
                       EffectsPool effectsPool, Score score) { /* do nothing */ }

    /**
     * Draws this bon a graphics context.
     *
     * @param graphics a graphics context on which to draw tank
     */
    public void draw(Graphics graphics) {
        GRAPHICS_COMPONENT.draw(this, graphics);
    }

    /**
     * Obtains allegiance of this.
     *
     * @return allegiance of this
     */
    public Allegiance getAllegiance() { return ALLEGIANCE; }

    /**
     * Obtains tank texture.
     *
     * @return texture of this
     */
    public BufferedImage getTexture() { return texture; }

    /**
     * Obtains x coordinate.
     *
     * @return fron x coordinate
     */
    public double getX() { return x; }

    /**
     * Sets the value of x to new value.
     *
     * @param x new value of x coordinate
     */
    public void setX(double x) { this.x = x; }

    /**
     * Obtains y coordinate.
     *
     * @return from y coordinate
     */
    public double getY() { return y; }

    /**
     * Sets the value of y to new value.
     *
     * @param y new value of y coordinate
     */
    public void setY(double y) { this.y = y; }

    /**
     * Obtains width of this.
     *
     * @return width of this
     */
    public int getWidth() { return width; }

    /**
     * Obtains height of this.
     *
     * @return height of this
     */
    public int getHeight() { return height; }

    /**
     * Obtains the bounds of this.
     *
     * @return rectangle representing the bounds of this
     */
    public Rectangle getBounds() { return new Rectangle((int) x, (int) y, width, height); }

    /**
     * Checks collision this with an entity.
     *
     * @param entity entity with which to check collision with this
     * @return a flag that is true, if the entity bounds do not intersect.
     */
    public boolean checkCollision(Entity entity) { return this != entity && getBounds().intersects(entity.getBounds()); }

    /**
     * Checks collision this with a rectangle.
     *
     * @param rectangle entity with which to check collision with this
     * @return a flag that is true, if the rectangle do not intersect.
     */
    public boolean checkCollision(Rectangle rectangle) { return getBounds().intersects(rectangle); }
}
