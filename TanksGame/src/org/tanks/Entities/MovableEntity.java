package org.tanks.Entities;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Graphics.GraphicsComponent;
import org.tanks.Entities.Components.Physics.PhysicsComponent;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code MovableEntity} class is an extension of the {@code Entity} class which must contain
 * an instance of all 3 components graphics, input and physics.
 *
 * @see Entity
 * @see GraphicsComponent
 * @see org.tanks.Entities.Components.Input.InputComponent
 * @see org.tanks.Entities.Components.Physics.PhysicsComponent
 * @author Beldiman Vladislav
 */
public abstract class MovableEntity extends Entity {
    /**
     * flag marking this for destruction it's by manager
     */
    protected boolean destroyed;

    /**
     * velocity scale
     */
    protected double velScale;

    /**
     * physics component
     */
    protected PhysicsComponent physicsComponent;

    /**
     * Sole constructor
     *
     * @param allegiance new allegiance
     * @param physicsComponent
     * @param texture buffered image for texture
     * @param x new x coordinates
     * @param y new y coordinates
     * @param width new width
     * @param height new height
     * @param velScale velocity scale
     * @see Allegiance
     */
    public MovableEntity(Allegiance allegiance, PhysicsComponent physicsComponent,
                         BufferedImage texture, double x, double y, int width, int height,
                         double velScale) {
        super(allegiance, texture, x, y, width, height);
        this.velScale = velScale;
        this.physicsComponent = physicsComponent;
    }

    @Override
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager, PlayerManager playerManager, EnemyPool enemyPool, ProjectileManager projectileManager, PickupsPool pickupsPool, EffectsPool effectsPool, Score score) {
        physicsComponent.move(this);
        physicsComponent.checkCollision(this, worldBounds, map, obstacleManager, playerManager, enemyPool, projectileManager, pickupsPool);
        physicsComponent.solveCollision(this, effectsPool, score);
    }

    /**
     * Obtains the velocity scale.
     * @return velocity scale
     */
    public double getVelScale() { return velScale; }

    /**
     * Checks the destroyed flag.
     *
     * @return flag that is true, if destroyed is true
     */
    public boolean destroyed() {
        return destroyed;
    }

    /**
     * Marks this for destruction by it's manager. Movable entities do not have health points so this
     * is instantly destroyed.
     *
     * @param amount the amount of health points to take away
     */
    public void damage(int amount) {
        destroyed = true;
    }

    /**
     * Obtains unit velocity on x axis.
     *
     * @return unit velocity on x axis
     */
    public abstract double getUnitVelX();

    /**
     * Obtains unit velocity on y axis.
     *
     * @return unit velocity on y axis
     */
    public abstract double getUnitVelY();


    /**
     * Delegates call to physics component
     *
     * @param entity an entity to check collision with
     * @return a flag that is true, if a collision occurred
     */
    public boolean checkCollision(Entity entity) {
        return physicsComponent.checkCollision(this, entity);
    }

    /**
     * Delegates call to physics component
     *
     * @param rectangle a rectangle to check collision with
     * @return a flag that is true, if a collision occurred
     */
    public boolean checkCollision(Rectangle rectangle) {
        return physicsComponent.checkCollision(this, rectangle);
    }

    /**
     * Delegates call to physics component
     *
     * @param projectile a projectile to check collision with
     * @return a flag that is true, if a collision occurred
     */
    public boolean checkDestructiveCollision(Projectile projectile) {
        return physicsComponent.checkDestructiveCollision(this, projectile);
    }
}
