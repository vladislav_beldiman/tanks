package org.tanks.Entities.Objects.Projectiles;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Allegiance;

/**
 * The {@code Rocket} class is an extension of the {@code Projectile} class for rocket type objects.
 *
 * @see Projectile
 * @author Beldiman Vladilsav
 */
public class Rocket extends Projectile {
    /**
     * base velocity scale
     */
    private static final double BASE_VEL_SCALE = 3.5;

    /**
     * base dealt damage
     */
    private static final int BASE_DAMAGE = 3;

    /**
     * Sole contructor.
     *
     * @param allegiance allegiance of this
     * @param x x coordinate of this
     * @param y y coordinate of this
     * @param direction direction of this
     */
    public Rocket(Allegiance allegiance, double x, double y, Direction direction) {
        super(allegiance, Assets.ROCKET[direction.ordinal()], x, y,
                Assets.ROCKET_WIDTH, Assets.ROCKET_HEIGHT, direction, BASE_VEL_SCALE, BASE_DAMAGE);
    }

    @Override
    public ProjectileType getProjectileType() {
        return ProjectileType.ROCKET;
    }
}
