package org.tanks.Entities.Objects.Projectiles;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Allegiance;

/**
 * The {@code Shell} class is an extension of the {@code Projectile} class for shell type objects.
 *
 * @see Projectile
 * @author Beldiman Vladilsav
 */
public class Shell extends Projectile {
    /**
     * base velocity scale
     */
    private static final double BASE_VEL_SCALE = 4;


    /**
     * base dealt damage
     */
    private static final int BASE_DAMAGE = 1;

    /**
     * Sole contructor.
     *
     * @param allegiance allegiance of this
     * @param x x coordinate of this
     * @param y y coordinate of this
     * @param direction direction of this
     */
    public Shell(Allegiance allegiance, double x, double y, Direction direction) {
        super(allegiance, Assets.SHELL[direction.ordinal()], x, y,
                Assets.SHELL_WIDTH, Assets.SHELL_HEIGHT, direction, BASE_VEL_SCALE, BASE_DAMAGE);
    }

    @Override
    public ProjectileType getProjectileType() {
        return ProjectileType.SHELL;
    }
}