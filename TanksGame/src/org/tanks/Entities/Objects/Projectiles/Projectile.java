package org.tanks.Entities.Objects.Projectiles;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Components.Physics.ProjectilePhysicsComponent;
import org.tanks.Entities.MovableEntity;
import org.tanks.Entities.Allegiance;

import java.awt.image.BufferedImage;

/**
 * The {@code Projectile} class is an extension of the {@code MovableEntity} class for projectile type objects.
 *
 * @see MovableEntity
 * @author Beldiman Vladislav
 */
public abstract class Projectile extends MovableEntity {
    private final Direction DIRECTION;
    private final int damage;

    /**
     * Sole constructor
     *
     * @param allegiance allegiance of this
     * @param texture texture of this
     * @param x x coordinate of this
     * @param y y coordinate of this
     * @param width width of this
     * @param height height of this
     * @param direction direction of this
     * @param velScale velocity scale of this
     * @param damage damage dealt by this
     * @see Allegiance
     * @see Direction
     */
    public Projectile(Allegiance allegiance, BufferedImage texture, double x, double y, int width,
                      int height, Direction direction, double velScale, int damage) {
        super(allegiance, new ProjectilePhysicsComponent(), texture, x, y, width, height, velScale);
        this.DIRECTION = direction;
        this.damage = damage;
    }

    @Override
    public int getWidth() {
        return switch (DIRECTION) {
            // NORMAL
            case UP, DOWN -> width;
            // FLIPPED
            case LEFT, RIGHT -> height;
            /* intentionally left STILL out. Projectile should not be STILL */
            default -> 0;
        };
    }

    @Override
    public int getHeight() {
        return switch (DIRECTION) {
            // NORMAL
            case UP, DOWN -> height;
            // FLIPPED
            case LEFT, RIGHT -> width;
            /* intentionally left STILL out. Projectile should not be STILL */
            default -> 0;
        };
    }

    @Override
    public double getUnitVelX() {
        return DIRECTION.getUnitVelX();
    }

    @Override
    public double getUnitVelY() {
        return DIRECTION.getUnitVelY();
    }

    /**
     * Obtain damage dealt by projectile.
     *
     * @return damage dealt by projectile
     */
    public int getDamage() { return damage; }

    /**
     * Obtain projectile flying direction.
     *
     * @return direction of projectile flight
     * @see Direction
     */
    public Direction getDirection() { return DIRECTION; }

    /**
     * Obtain projectile type.
     *
     * @return projectile type
     * @see ProjectileType
     */
    public abstract ProjectileType getProjectileType();
}
