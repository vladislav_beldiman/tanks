package org.tanks.Entities.Objects.Projectiles;

/**
 * The {@code ProjectileType} is an enum of available projectile types.
 *
 * @see Projectile
 * @author Beldiman Vladislav
 */
public enum ProjectileType {
    /**
     * shell type
     */
    SHELL(),

    /**
     * rocket type
     */
    ROCKET()
}
