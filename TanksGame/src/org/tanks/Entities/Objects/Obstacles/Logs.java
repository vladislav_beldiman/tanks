package org.tanks.Entities.Objects.Obstacles;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Allegiance;

/**
 * The {@code Logs} class is an extension of the {@code Obstacle} class for logs obstacles.
 *
 * @see Obstacle
 * @author Beldiman Vladislav
 */
public class Logs extends Obstacle {
    /**
     * Sole constructor
     *
     * @param x x coordinate of the obstacle
     * @param y y coordinate of the obstacle
     */
    public Logs(double x, double y) {
        super(Allegiance.Neutral, Assets.LOGS, x, y, Assets.LOGS_WIDTH, Assets.LOGS_HEIGHT);
    }

    @Override
    public ObstacleType getObstacleType() {
        return ObstacleType.LOGS;
    }
}