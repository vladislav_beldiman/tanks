package org.tanks.Entities.Objects.Obstacles;

import org.tanks.Entities.Components.Physics.ImmutablePhysicsComponent;
import org.tanks.Entities.Components.Physics.PhysicsComponent;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Allegiance;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Obstacle} class is an extension of the {@code Entity} class for obstacles on the map.
 *
 * @see Entity
 * @author Beldiman Vladislav
 */
public abstract class Obstacle extends Entity {
    protected static final PhysicsComponent PHYSICS_COMPONENT = new ImmutablePhysicsComponent();
    /**
     * Sole constructor
     *
     * @param allegiance allegiance of obstacle
     * @param texture texture of obstacle
     * @param x x coordinate of obstacle
     * @param y y coordinate of obstacle
     * @param width width of obstacle
     * @param height height of obstacle
     */
    public Obstacle(Allegiance allegiance, BufferedImage texture, double x, double y,
                    int width, int height) {
        super(allegiance, texture, x, y, width, height);
    }

    /**
     * Obtains obstacle type.
     *
     * @return obstacle type
     */
    public abstract ObstacleType getObstacleType();

    /**
     * Delegates call to physics component
     *
     * @param entity an entity to check collision with
     * @return a flag that is true, if a collision occurred
     */
    public boolean checkCollision(Entity entity) {
        return PHYSICS_COMPONENT.checkCollision(this, entity);
    }

    /**
     * Delegates call to physics component
     *
     * @param rectangle a rectangle to check collision with
     * @return a flag that is true, if a collision occurred
     */
    public boolean checkCollision(Rectangle rectangle) {
        return PHYSICS_COMPONENT.checkCollision(this, rectangle);
    }
}
