package org.tanks.Entities.Objects.Obstacles;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Allegiance;

/**
 * The {@code Tree} class is an extension of the {@code Obstacle} class for tree obstacles.
 *
 * @see Obstacle
 * @author Beldiman Vladislav
 */
public class Tree extends Obstacle {
    /**
     * Sole constructor
     *
     * @param x x coordinate of the obstacle
     * @param y y coordinate of the obstacle
     */
    public Tree(double x, double y) {
        super(Allegiance.Neutral, Assets.TREE, x, y, Assets.TREE_WIDTH, Assets.TREE_HEIGHT);
    }

    @Override
    public ObstacleType getObstacleType() {
        return ObstacleType.TREE;
    }
}
