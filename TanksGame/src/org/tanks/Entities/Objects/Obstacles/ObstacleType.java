package org.tanks.Entities.Objects.Obstacles;

/**
 * The {@code ObstacleType} enum specifies available obstacle types.
 *
 * @see Obstacle
 * @author Beldiman Vladislav
 */
public enum ObstacleType {
    /**
     * hedgehog type
     */
    HEDGEHOG,

    /**
     * logs type
     */
    LOGS,

    /**
     * tree type
     */
    TREE
}
