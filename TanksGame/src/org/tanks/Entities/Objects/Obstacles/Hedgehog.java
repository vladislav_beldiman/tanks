package org.tanks.Entities.Objects.Obstacles;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Allegiance;

/**
 * The {@code Hedgehog} class is an extension of the {@code Obstacle} class for hedgehog obstacles.
 *
 * @see Obstacle
 * @author Beldiman Vladislav
 */
public class Hedgehog extends Obstacle {
    /**
     * Sole constructor
     *
     * @param x x coordinate of the obstacle
     * @param y y coordinate of the obstacle
     */
    public Hedgehog(double x, double y) {
        super(Allegiance.Neutral, Assets.HEDGEHOG, x, y, Assets.HEDGEHOG_WIDTH, Assets.HEDGEHOG_HEIGHT);
    }

    @Override
    public ObstacleType getObstacleType() {
        return ObstacleType.HEDGEHOG;
    }
}
