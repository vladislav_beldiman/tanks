package org.tanks.Entities.Objects;

import org.tanks.Assets.Assets;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.InputComponent;
import org.tanks.Entities.Components.Physics.PhysicsComponent;
import org.tanks.Entities.Components.Physics.TankPhysicsComponent;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.MovableEntity;
import org.tanks.Entities.Objects.TankStates.GunStates.GunState;
import org.tanks.Entities.Objects.TankStates.GunStates.ReadyState;
import org.tanks.Entities.Objects.TankStates.LevelUpStates.Active;
import org.tanks.Entities.Objects.TankStates.LevelUpStates.Inactive;
import org.tanks.Entities.Objects.TankStates.LevelUpStates.LevelUpState;
import org.tanks.Entities.Objects.TankStates.MoveStates.DirectionState;
import org.tanks.Entities.Objects.TankStates.MoveStates.RightState;
import org.tanks.Entities.Allegiance;
import org.tanks.Input.KeyInput;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Tank} class is an extension of the {@code MovableEntity} class that represents tank type
 * entities. It is used for both player and enemy instances.
 */
public class Tank extends MovableEntity {
    /**
     * base velocity scale of this
     */
    private static final double BASE_VEL_SCALE = 1.5;

    /**
     * array that holds default health point values for each tank level
     */
    private static final int[] hpAtLevel = {1, 2, 4};

    /**
     * current tank level
     */
    private int level = 0;

    /**
     * current tank health points
     */
    private int hp;

    /**
     * difficulty scale
     */
    private final int scale;

    /**
     * input component of this
     */
    private InputComponent inputComponent;

    /**
     * direction state of this. Defaults to right state.
     */
    private DirectionState directionState = new RightState();

    /**
     * gun state of this. Defaults to ready state
     */
    private GunState gunState = new ReadyState();

    /**
     * level up state of this. Defaults to inactive
     */
    private LevelUpState levelUpState = new Inactive();

    /**
     * Sole constructor
     *
     * @param allegiance allegiance of the tank
     * @param inputComponent input component of the tank
     * @param texture texture of the tank
     * @param x x coordinate of the tank
     * @param y y coordinate of the tank
     * @param scale difficulty scale for the tank
     */
    public Tank(Allegiance allegiance, InputComponent inputComponent, BufferedImage texture,
                double x, double y, int scale) {

        super(allegiance, new TankPhysicsComponent(), texture, x, y,
                Assets.TANK_WIDTH, Assets.TANK_HEIGHT, BASE_VEL_SCALE);

        this.scale = scale;
        // enemies have higher hp at higher difficulties
        hp = hpAtLevel[0] * scale;
        this.inputComponent = inputComponent;
        directionState.setTexture(this);
    }

    @Override
    public void handleInput(KeyInput input) {
        inputComponent.handleInput(this, input);
    }

    @Override
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager,
                       PlayerManager playerManager, EnemyPool enemyPool,
                       ProjectileManager projectileManager, PickupsPool pickupsPool,
                       EffectsPool effectsPool, Score score) {

        super.update(worldBounds, map, obstacleManager, playerManager, enemyPool,
                        projectileManager, pickupsPool, effectsPool, score);

        setGunState(gunState.update(this, projectileManager, effectsPool));
        levelUpState.update(this);
    }

    /**
     * Obtains direction state of this.
     *
     * @return direction state of this
     * @see org.tanks.Entities.Objects.TankStates.MoveStates.DirectionState
     */
    public DirectionState getDirectionState() {
        return directionState;
    }

    /**
     * Sets direction state of this, if next state is not null.
     *
     * @param nextState direction state to be set
     * @see org.tanks.Entities.Objects.TankStates.MoveStates.DirectionState
     */
    public void setDirectionState(DirectionState nextState) {
        if (nextState != null) {
            directionState = nextState;
            directionState.enter(this);
        }
    }

    /**
     * Obtains gun state of this.
     *
     * @return gun state of this
     * @see org.tanks.Entities.Objects.TankStates.GunStates.GunState
     */
    public GunState getGunState() {
        return gunState;
    }

    /**
     * Sets gun state of this, if next state is not null.
     *
     * @param nextState gun state to be set
     * @see org.tanks.Entities.Objects.TankStates.GunStates.GunState
     */
    public void setGunState(GunState nextState) {
        if (nextState != null) {
            gunState = nextState;
            gunState.enter(this);
        }
    }

    /**
     * Sets level up state of this, if next state is not null.
     *
     * @param effectsPool an effects pool to play level up animations from
     * @param score a score object to add to
     * @param nextState level up state to be set
     */
    public void setLevelUpState(EffectsPool effectsPool, Score score, LevelUpState nextState) {
        if (nextState != null) {
            levelUpState.exit(this);
            levelUpState = nextState;
            levelUpState.enter(effectsPool, score,this);
        }
    }

    /**
     * Sets input component of this.
     *
     * @param inputComponent input component to be set
     */
    public void setInputComponent(InputComponent inputComponent) {
        this.inputComponent = inputComponent;
    }

    /**
     * Obtains input component of this.
     *
     * @return input component of this
     */
    public InputComponent getInputComponent() {
        return inputComponent;
    }

    /**
     * Sets physics component of this.
     *
     * @param physicsComponent physics component to be set
     */
    public void setPhysicsComponent(PhysicsComponent physicsComponent) {
        this.physicsComponent = physicsComponent;
    }

    /**
     * Obtains physics component of this.
     *
     * @return physics component of this
     */
    public PhysicsComponent getPhysicsComponent() {
        return physicsComponent;
    }

    /**
     * Obtains tank level.
     *
     * @return level of the tank
     */
    public int getLevel() { return level; }

    /**
     * Sets the level of this.
     *
     * @param level new level
     */
    public void setLevel(int level) { this.level = level; }

    /**
     * Increments level of this.
     */
    public void incLevel() { level++; }

    /**
     * Levels up this by setting the level up state to active.
     *
     * @param effectsPool effects pool to play level up animation from
     * @param score a score object to add to
     */
    public void levelUp(EffectsPool effectsPool, Score score) {
        setLevelUpState(effectsPool, score, new Active());
    }

    /**
     * Restores health points of this to their default value per level (scaled for enemies by
     * difficulty).
     */
    public void healUp() {
        hp = hpAtLevel[level] * scale;
    }

    /**
     * Obtains current health points.
     *
     * @return current health points
     */
    public int getHp() { return hp; }

    /**
     * Sets health points to a given value.
     *
     * @param hp new value of health points
     */
    public void setHp(int hp) { this.hp = hp; }

    @Override
    public void damage(int amount) {
        hp -= amount;
        if (hp <= 0) {
            destroyed = true;
        }
    }

    /**
     * Unmarks this for destruction.
     */
    public void repair() {
        destroyed = false;
    }

    /**
     * Respawns this at another location.
     *
     * @param inputComponent new input component
     * @param x new x coordinates
     * @param y new y coordinates
     */
    public void respawn(InputComponent inputComponent, double x, double y) {
        repair();
        this.x = x;
        this.y = y;
        this.inputComponent = inputComponent;
        level = 0;
        healUp();
        setDirectionState(new RightState());
        reloadTexture();

        setGunState(new ReadyState());
    }

    /**
     * Reloads texture for current direction state.
     */
    public void reloadTexture() {
        directionState.setTexture(this);
    }

    /**
     * Sets texture to another value.
     *
     * @param texture buffered image of the new texture
     */
    public void setTexture(BufferedImage texture) { this.texture = texture; }

    /**
     * Obtains this' difficulty scale.
     *
     * @return a number representing the difficulty scale of this
     */
    public int getScale() { return scale; }

    @Override
    public Rectangle getBounds() { return new Rectangle((int) x, (int) y, width, height); }

    @Override
    public double getUnitVelX() {
        return directionState.getUnitVelX();
    }

    @Override
    public double getUnitVelY() {
        return directionState.getUnitVelY();
    }
}
