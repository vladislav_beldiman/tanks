package org.tanks.Entities.Objects.TankStates.LevelUpStates;

import org.tanks.AudioService.SoundID;
import org.tanks.Effects.Effect;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.InputComponent;
import org.tanks.Entities.Components.Input.NullInputComponent;
import org.tanks.Entities.Components.Physics.ImmutablePhysicsComponent;
import org.tanks.Entities.Components.Physics.NullPhysicsComponent;
import org.tanks.Entities.Components.Physics.PhysicsComponent;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;
import org.tanks.Score;

/**
 * The {@code Active} class is an extension of {@code LevelUpState} class that signifies that the
 * tank is currently leveling up.
 *
 * @see LevelUpState
 * @author Beldiman Vladislav
 */
public class Active extends LevelUpState {
    /**
     * input component that is decoupled on entry and recoupled on exit
     */
    InputComponent inputComponent;

    /**
     * physics component that is decoupled on entry and recoupled on exit
     */
    PhysicsComponent physicsComponent;

    /**
     * standard level up time
     */
    private static final int LEVEL_UP_TIME = Effect.ANIMATION_DURATION;

    /**
     * level up time left
     */
    private int cornerTime;

    @Override
    public void enter(EffectsPool effectsPool, Score score, Tank tank) {
        if (tank.getAllegiance() == Allegiance.Player) {
            score.addScore(200);
            Game.LOCATOR.getAudioService().playSound(SoundID.LEVEL_UP);
        }

        effectsPool.play(EffectsPool.LEVEL_UP, (int) tank.getX(), (int) tank.getY());

        if (tank.getLevel() < 2) {
            tank.incLevel();
            tank.getDirectionState().setTexture(tank);
        }

        tank.healUp();

        // decouple input and physics components and replace them with null instances
        // this makes tank unable to move or to be damaged during animation
        inputComponent = tank.getInputComponent();
        physicsComponent = tank.getPhysicsComponent();

        tank.setInputComponent(new NullInputComponent());
        tank.setPhysicsComponent(new ImmutablePhysicsComponent());

        cornerTime = LEVEL_UP_TIME;
    }

    @Override
    public void update(Tank tank) {
        cornerTime--;

        if (cornerTime <= 0) {
            tank.setLevelUpState(null, null, new Inactive());
        }
    }

    @Override
    public void exit(Tank tank) {
        // recouple input and physics components
        tank.setInputComponent(inputComponent);
        tank.setPhysicsComponent(physicsComponent);
    }
}
