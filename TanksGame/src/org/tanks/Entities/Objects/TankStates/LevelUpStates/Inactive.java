package org.tanks.Entities.Objects.TankStates.LevelUpStates;

/**
 * The {@code Inactive} class is an extension of {@code LevelUpState} class that signifies that the
 * tank is not currently leveling up.
 *
 * @see LevelUpState
 * @author Beldiman Vladislav
 */
public class Inactive extends LevelUpState {
}
