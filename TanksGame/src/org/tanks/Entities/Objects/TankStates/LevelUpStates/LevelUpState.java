package org.tanks.Entities.Objects.TankStates.LevelUpStates;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Score;
import org.tanks.World;

/**
 * The {@code LevelUpState} is an abstract class for tank level up states.
 *
 * @see Tank
 * @author Beldiman Vladislav
 */
public abstract class LevelUpState {
    /**
     * Executed on state entry.
     *
     * @param effectsPool effects pool to play level up animation from
     * @param score a score object to add to
     * @param tank a tank entering this state
     */
    public void enter(EffectsPool effectsPool, Score score, Tank tank) {
        /* do nothing */
    }

    /**
     * Updates this.
     *
     * @param tank a tank for which to update level up state
     */
    public void update(Tank tank) {
        /* do nothing */
    }

    /**
     * Executed on state exit.
     *
     * @param tank a tank exiting this state
     */
    public void exit(Tank tank) {
        /* do nothing */
    }
}
