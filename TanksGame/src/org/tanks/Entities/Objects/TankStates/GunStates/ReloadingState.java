package org.tanks.Entities.Objects.TankStates.GunStates;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Tank;

/**
 * The {@code ReloadingState} class is an extension of {@code GunState} class that is used to signify
 * that the tank is reloading it's gun.
 *
 * @see GunState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class ReloadingState extends GunState {
    /**
     * standard reload time
     */
    private static final int RELOAD_TIME = 90;

    /**
     * current gun cooldown
     */
    private int cooldown;

    @Override
    public void enter(Tank tank) {
        // the cooldown is reduced for enemies at higher difficulties
        cooldown = RELOAD_TIME / tank.getScale();
    }

    @Override
    public GunState update(Tank tank, ProjectileManager projectileManager, EffectsPool effectsPool) {
        cooldown--;
        if (cooldown <= 0) {
            return new ReadyState();
        }
        return null;
    }
}
