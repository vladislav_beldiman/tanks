package org.tanks.Entities.Objects.TankStates.GunStates;

import org.tanks.AudioService.SoundID;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Projectiles.Shell;
import org.tanks.Entities.Objects.Projectiles.Rocket;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;

/**
 * The {@code ShootState} class is an extension of {@code GunState} class that is used to signify
 * that the tank is shooting from it's gun.
 *
 * @see GunState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class ShootState extends GunState {
    @Override
    public GunState update(Tank tank, ProjectileManager projectileManager, EffectsPool effectsPool) {
        Allegiance allegiance = tank.getAllegiance  ();
        Direction direction = tank.getDirectionState().getDirection();
        int level = tank.getLevel();
        double x = tank.getX();
        double y = tank.getY();
        int width = tank.getWidth();
        int height = tank.getHeight();

        // calculating subjective spawn location for projectiles
        double spawnX;
        double spawnY;
        int projectileOffset = 13;

        spawnX = switch (direction) {
            case UP, DOWN -> x + width / 2. - projectileOffset / 2.;
            case LEFT -> x - projectileOffset;
            case RIGHT -> x + width;
            default -> -1;
        };

        spawnY = switch (direction) {
            case UP -> y - projectileOffset;
            case DOWN -> y + height;
            case LEFT, RIGHT -> y + height / 2. - projectileOffset / 2.;
            default -> -1;
        };

        if (level == 0) {
            // level 0 tank shoots 1 shell
            projectileManager.addProjectile(new Shell(allegiance, spawnX, spawnY, direction));
        }

        if (level == 1) {
            // level 1 tank shoots 2 shells
            double spawnX2 = spawnX;
            double spawnY2 = spawnY;
            int doubleBarrelOffset = 4;

            switch (direction) {
                case UP, DOWN -> {
                    spawnX -= doubleBarrelOffset;
                    spawnX2 += doubleBarrelOffset;
                }
                case LEFT, RIGHT -> {
                    spawnY -= doubleBarrelOffset;
                    spawnY2 += doubleBarrelOffset;
                }
            }

            projectileManager.addProjectile(new Shell(allegiance, spawnX, spawnY, direction));
            projectileManager.addProjectile(new Shell(allegiance, spawnX2, spawnY2, direction));

            effectsPool.play(EffectsPool.FLASH, (int) spawnX2, (int) spawnY2);
        }

        if (level == 2) {
            // level 2 tank shoots 1 rocket
            projectileManager.addProjectile(new Rocket(allegiance, spawnX, spawnY, direction));
        }

        // common animations and sound effects
        effectsPool.play(EffectsPool.FLASH, (int) spawnX, (int) spawnY);
        Game.LOCATOR.getAudioService().playSound(SoundID.FIRE);

        return new ReloadingState();
    }
}
