package org.tanks.Entities.Objects.TankStates.GunStates;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Tank;

/**
 * The {@code GunState} class is an abstract class for the states of a tank's gun.
 *
 * @see Tank
 * @author Beldiman Vladislav
 */
public abstract class GunState {
    /**
     * Method executed on state entry.
     *
     * @param tank tank entering this state
     * @see Tank
     */
    public void enter(Tank tank) {
        /* do nothing */
    }

    /**
     * Updates this.
     *
     * @param tank a tank updating this an instance of this
     * @param projectileManager a projectile manager to add shot projectiles to
     * @param effectsPool an effects pool to play flash animations from
     * @return the next gun state or null, if it remains unchanged
     */
    public GunState update(Tank tank, ProjectileManager projectileManager, EffectsPool effectsPool) {
        /* do nothing */
        return null;
    }

    /**
     * Shoots a projectile type object from the tank.
     *
     * @param tank a tank to shoot from
     * @return the next gun state or null, if it remains unchanged
     */
    public GunState shootFrom(Tank tank) {
        /* do nothing */
        return null;
    }
}
