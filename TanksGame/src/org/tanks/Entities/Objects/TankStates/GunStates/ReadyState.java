package org.tanks.Entities.Objects.TankStates.GunStates;

import org.tanks.Entities.Objects.Tank;

/**
 * The {@code ReadyState} class is an extension of {@code GunState} class that is used to signify that the tank is
 * ready to shoot.
 *
 * @see GunState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class ReadyState extends GunState {

    @Override
    public GunState shootFrom(Tank tank) {
        return new ShootState();
    }
}
