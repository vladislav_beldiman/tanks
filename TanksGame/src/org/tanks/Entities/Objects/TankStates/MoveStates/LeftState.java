package org.tanks.Entities.Objects.TankStates.MoveStates;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Input.KeyInput;

/**
 * The {@code LeftState} class is an extension of the {@code DirectionState} class signifying that the
 * {@code Tank} it belongs to is moving leftwards.
 *
 * @see DirectionState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class LeftState extends DirectionState {

    @Override
    public DirectionState handleInput(Tank tank, KeyInput input) {
        if (input.leftPressed()) {
            return null;
        }

        if (input.upPressed()) {
            return new UpState();
        }

        if (input.downPressed()) {
            return new DownState();
        }

        if (input.rightPressed()) {
            return new RightState();
        }

        return new StillState(this);
    }

    @Override
    public Direction getDirection() {
        return Direction.LEFT;
    }
}
