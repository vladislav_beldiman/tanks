package org.tanks.Entities.Objects.TankStates.MoveStates;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Input.KeyInput;

/**
 * The {@code StillState} class is an extension of the {@code DirectionState} class signifying that the
 * {@code Tank} it belongs to is not moving. In this state the tank will return the texture, width and
 * height of the state before this.
 *
 * @see DirectionState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class StillState extends DirectionState {
    /**
     * old state reference
     */
    private final DirectionState oldState;

    /**
     * Sole constructor
     *
     * @param oldState old state reference
     * @see DirectionState
     */
    public StillState(DirectionState oldState) {
        this.oldState = oldState;
    }

    @Override
    public DirectionState handleInput(Tank tank, KeyInput input) {
        if (input.upPressed()) {
            return new UpState();
        }

        if (input.downPressed()) {
            return new DownState();
        }

        if (input.leftPressed()) {
            return new LeftState();
        }

        if (input.rightPressed()) {
            return new RightState();
        }

        return null;
    }

    @Override
    public Direction getDirection() {
        return oldState.getDirection();
    }

    @Override
    public double getUnitVelX() {
        return Direction.STILL.getUnitVelX();
    }

    @Override
    public double getUnitVelY() {
        return Direction.STILL.getUnitVelY();
    }
}

