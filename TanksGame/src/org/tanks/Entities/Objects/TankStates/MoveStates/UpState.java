package org.tanks.Entities.Objects.TankStates.MoveStates;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Input.KeyInput;

/**
 * The {@code UpState} class is an extension of the {@code DirectionState} class signifying that the
 * {@code Tank} it belongs to is moving upwards.
 *
 * @see DirectionState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class UpState extends DirectionState {

    @Override
    public DirectionState handleInput(Tank tank, KeyInput input) {
        if (input.upPressed()) {
            return null;
        }

        if (input.downPressed()) {
            return new DownState();
        }

        if (input.leftPressed()) {
            return new LeftState();
        }

        if (input.rightPressed()) {
            return new RightState();
        }

        return new StillState(this);
    }

    @Override
    public Direction getDirection() {
        return Direction.UP;
    }
}
