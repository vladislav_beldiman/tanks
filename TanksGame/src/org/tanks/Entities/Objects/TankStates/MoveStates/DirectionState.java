package org.tanks.Entities.Objects.TankStates.MoveStates;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Input.KeyInput;

import java.awt.image.BufferedImage;

/**
 * The {@code DirectionState} class is an abstract state for states of tank's movement.
 *
 * @see Tank
 * @author Beldiman Vladislav
 */
public abstract class DirectionState {
    /**
     * Set state texture and dimensions on entry.
     *
     * @param tank a tank entering this state
     */
    public void enter(Tank tank) {
        setTexture(tank);
    }

    /**
     * Handles input for a tank.
     *
     * @param tank a tank whose input is to be handled
     * @param input key input
     * @return a new direction state, if it is changed. Null otherwise
     */
    public abstract DirectionState handleInput(Tank tank, KeyInput input);

    /**
     * Obtains direction of the state.
     *
     * @return the direction of the state
     */
    public abstract Direction getDirection();

    /**
     * Sets tank's texture for this state.
     *
     * @param tank a tank
     */
    public void setTexture(Tank tank) {
        int level = tank.getLevel();
        int direction = getDirection().ordinal();

        BufferedImage newTexture = switch (tank.getAllegiance()) {
            case Player -> Assets.PLAYER[level][direction];
            case Enemy -> Assets.ENEMY[level][direction];
            default -> Assets.BLANK;
        };

        tank.setTexture(newTexture);
    }

    /**
     * Obtains unit velocity on x axis of this state.
     *
     * @return unit velocity on x axis of this state
     */
    public double getUnitVelX() { return getDirection().getUnitVelX(); }

    /**
     * Obtains unit velocity on x axis of this state.
     *
     * @return unit velocity on x axis of this state
     */
    public double getUnitVelY() { return getDirection().getUnitVelY(); }
}
