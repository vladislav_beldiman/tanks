package org.tanks.Entities.Objects.TankStates.MoveStates;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Input.KeyInput;

/**
 * The {@code RightState} class is an extension of the {@code DirectionState} class signifying that the
 * {@code Tank} it belongs to is moving rightwards.
 *
 * @see DirectionState
 * @see Tank
 * @author Beldiman Vladislav
 */
public class RightState extends DirectionState {

    @Override
    public DirectionState handleInput(Tank tank, KeyInput input) {
        if (input.rightPressed()) {
            return null;
        }

        if (input.upPressed()) {
            return new UpState();
        }

        if (input.downPressed()) {
            return new DownState();
        }

        if (input.leftPressed()) {
            return new LeftState();
        }

        return new StillState(this);
    }

    @Override
    public Direction getDirection() {
        return Direction.RIGHT;
    }
}
