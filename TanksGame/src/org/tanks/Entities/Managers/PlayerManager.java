package org.tanks.Entities.Managers;

import org.tanks.Assets.Assets;
import org.tanks.AudioService.SoundID;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.AIInputComponent;
import org.tanks.Entities.Components.Input.PlayerInputComponent;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;

/**
 * The {@code PlayerManager} class is used to manage the player {@code Tank}.
 *
 * @see Tank
 * @author Beldiman Vladislav
 */
public class PlayerManager {
    /**
     * default x spawn coordinate
     */
    public static final int PLAYER_SPAWN_X = 1200;

    /**
     * default y spawn coordinate
     */
    public static final int PLAYER_SPAWN_Y = 400;

    /**
     * player input component
     */
    private static final PlayerInputComponent PLAYER_INPUT_COMPONENT = new PlayerInputComponent();

    /**
     * player tank
     */
    private Tank player;

    /**
     * a flag that is true if the player has lost all of his lives
     */
    private boolean lost;

    /**
     * current lives count
     */
    private int lives;

    /**
     * Restarts this.
     */
    public void restart() {
        player = new Tank(Allegiance.Player, PLAYER_INPUT_COMPONENT, Assets.PLAYER[0][0],
                            PLAYER_SPAWN_X, PLAYER_SPAWN_Y, 1);

        lives = 3;
        lost = false;
    }

    /**
     * Load this from saved data.
     *
     * @param playerCoord a vector of points of player coordinates
     * @param playerLevel player level
     * @param playerHp player health points
     * @param playerLives player lives left
     */
    public void load(Point playerCoord, int playerLevel, int playerHp, int playerLives) {
        player = new Tank(Allegiance.Player, PLAYER_INPUT_COMPONENT, Assets.PLAYER[playerLevel][0],
                            playerCoord.getX(), playerCoord.getY(), 1);

        player.setLevel(playerLevel);
        player.setHp(playerHp);
        this.lives = playerLives;
    }

    /**
     * Handles input for the player.
     *
     * @param input key input
     */
    public void handleInput(KeyInput input) {
        player.handleInput(input);
    }

    /**
     * Updates tank player and player lives.
     *
     * @param worldBounds a rectangle representing world boundaries
     * @param map a map of the world with which to check collision
     * @param obstacleManager an obstacle manager of the world with which to check collision
     * @param playerManager a player manager of the world with which to check collision
     * @param enemyPool an enemy pool of the world with which to check collision
     * @param projectileManager a projectile manager of the world with which to check collision
     * @param pickupsPool a pickups pool of the world with which to check collision
     * @param effectsPool an effects pool from which to play destroy animations
     * @param score a score object to add to
     * @see Entity
     * @see Map
     * @see ObstacleManager
     * @see PlayerManager
     * @see EnemyPool
     * @see ProjectileManager
     * @see PickupsPool
     * @see EffectsPool
     * @see Score
     */
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager,
                       PlayerManager playerManager, EnemyPool enemyPool,
                       ProjectileManager projectileManager, PickupsPool pickupsPool,
                       EffectsPool effectsPool, Score score) {

        player.update(worldBounds, map, obstacleManager, playerManager, enemyPool,
                        projectileManager,pickupsPool, effectsPool, score);

        if (player.destroyed()) {
            effectsPool.play(EffectsPool.EXPLOSION, (int) player.getX(), (int) player.getY());
            Game.LOCATOR.getAudioService().playSound(SoundID.BLOW_UP);
            lives--;

            if (lives <= 0) {
                lost = true;
            }
            else {
                player.repair();
                player.respawn(player.getInputComponent(), PLAYER_SPAWN_X, PLAYER_SPAWN_Y);
            }
        }
    }

    /**
     * Draws player on a graphics context.
     *
     * @param graphics a graphics context to draw the player on
     */
    public void draw(Graphics graphics) {
        player.draw(graphics);
    }

    /**
     * Checks collision for the player with an entity.
     *
     * @param entity an entity to check collision with
     * @return a flag that is true if collision with the entity was detected
     * @see Entity
     */
    public boolean checkCollision(Entity entity) {
        return player.checkCollision(entity);
    }

    /**
     * Checks collision for the player with an rectangle.
     *
     * @param rectangle an entity to check collision with
     * @return a flag that is true if collision with the rectangle was detected
     */
    public boolean checkCollision(Rectangle rectangle) {
        return player.checkCollision(rectangle);
    }

    /**
     * Checks collision for the player with an projectile and sets player for destruction on collision.
     *
     * @param projectile an entity to check collision with
     * @return a flag that is true if collision with the projectile was detected
     */
    public boolean checkDestructiveCollision(Projectile projectile) {
        return player.checkDestructiveCollision(projectile);
    }

    /**
     * Obtains player coordinates.
     *
     * @return a point of the coordinates of the player
     */
    public Point getPlayerPosition() {
        return new Point((int) player.getX(), (int) player.getY());
    }

    /**
     * Obtains player level.
     *
     * @return the level of the player
     */
    public int getPlayerLevel() {
        return player.getLevel();
    }

    /**
     * Obtains player health points.
     *
     * @return player health points
     */
    public int getPlayerHp() {
        return player.getHp();
    }

    /**
     * Obtains player lives left.
     *
     * @return player lives left
     */
    public int getPlayerLives() { return lives; }

    /**
     * Obtains player lost status.
     *
     * @return a flag that is true if player has lost
     */
    public boolean lost() { return lost; }

    /**
     * Restarts this with a demo version of the player, that is the player is AI controlled.
     */
    public void demo() {
        player = new Tank(Allegiance.Player, new AIInputComponent(), Assets.PLAYER[0][0],
                            PLAYER_SPAWN_X, PLAYER_SPAWN_Y, 1);

        player.setLevel(2);
        player.healUp();
        player.getDirectionState().setTexture(player);
        lives = 9999;
        lost = false;
    }

}
