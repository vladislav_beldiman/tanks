package org.tanks.Entities.Managers;

import org.tanks.Assets.Assets;
import org.tanks.AudioService.SoundID;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.AIInputComponent;
import org.tanks.Entities.Components.Input.NullInputComponent;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * The {@code EnemyPool} class is an object pool for enemy {@code Tank} type objects. It keeps respawning enemies after
 * a certain delay unless it's limit is reached.
 *
 * @see Tank
 * @author Beldiman Vladislav
 */
public class EnemyPool {
    /**
     * the maximum amount of active enemies
     */
    private final int MAX_ENEMIES;

    /**
     * x coordinate of initial spawn
     */
    private static final int GRAVEYARD_X = -100;

    /**
     * y coordinate of initial spawn
     */
    private static final int GRAVEYARD_Y = -100;

    /**
     * spawn location column
     */
    private final int SPAWN_X = 5;

    /**
     * maximum y coordinate for spawn location
     */
    private final int MAX_SPAWN_HEIGHT;

    /**
     * maximum number of tries to find a valid spawn location for enemies
     */
    private static final int MAX_SPAWN_TRIES = 100;

    /**
     * minimum spawn cooldown delay given in update cycles
     */
    private static final int MIN_DELAY = 180;

    /**
     * maximum spawn cooldown delay given in update cycles
     */
    private static final int MAX_COOLDOWN = 300;

    /**
     * current cooldown value given in update cycles
     */
    private int cooldown;

    /**
     * random used for spawn location generation
     */
    private final Random random = new Random();

    /**
     * first inactive enemy in vector pool
     */
    private int firstInactive;

    /**
     * vector pool of tank enemies
     */
    private Vector<Tank> enemies;

    /**
     * difficulty scale
     */
    private int scale;

    /**
     * Sole constructor.
     *
     * @param MAX_ENEMIES maximum active enemies
     * @param MAX_SPAWN_HEIGHT maximum y coordinate for spawn location
     */
    public EnemyPool(int MAX_ENEMIES, int MAX_SPAWN_HEIGHT) {
        this.MAX_ENEMIES = MAX_ENEMIES;
        this.MAX_SPAWN_HEIGHT = MAX_SPAWN_HEIGHT;
    }

    /**
     * Restarts enemy pool with a number of initial active enemies of a certain difficulty.
     *
     * @param initialEnemies number of initial active enemies
     * @param scale difficulty scale
     */
    public void restart(int initialEnemies, int scale) {
        this.scale = scale;
        firstInactive = initialEnemies;
        enemies = new Vector<>(MAX_ENEMIES * scale);

        for (int i = 0; i < MAX_ENEMIES * scale; i++) {
            enemies.add(new Tank(Allegiance.Enemy, new AIInputComponent(), Assets.ENEMY[0][0],
                        GRAVEYARD_X, GRAVEYARD_Y, scale));
        }

        // make first firstInactive enemies active
        for (int i = 0; i < firstInactive; i++) {
            respawn(i, SPAWN_X, i * (Assets.TANK_WIDTH + 40));
        }

        cooldown = nextCooldown();
    }

    /**
     * Load enemy pool from save data.
     *
     * @param enemiesCoord a vector of points of enemy coordinates
     * @param enemiesLevel a vector of enemy levels
     * @param enemiesHp a vector of enemy health points
     * @param scale difficulty scale
     */
    public void load(Vector<Point> enemiesCoord, Vector<Integer> enemiesLevel,
                     Vector<Integer> enemiesHp, int scale) {

        this.scale = scale;
        firstInactive = enemiesCoord.size();
        enemies = new Vector<>(MAX_ENEMIES * scale);

        for (int i = 0; i < MAX_ENEMIES * scale; i++) {
            enemies.add(new Tank(Allegiance.Enemy, new AIInputComponent(), Assets.ENEMY[0][0],
                        GRAVEYARD_X, GRAVEYARD_Y, scale));
        }

        for (int i = 0; i < firstInactive; i++) {
            Point point = enemiesCoord.get(i);
            respawn(i, point.getX(), point.getY());

            Tank enemy = enemies.get(i);
            int level = enemiesLevel.get(i);
            enemy.setLevel(level);
            enemy.reloadTexture();
            enemy.setHp(enemiesHp.get(i));
        }

        cooldown = nextCooldown();
    }

    /**
     * Handles input for active enemies.
     *
     * @param input key input
     */
    public void handleInput(KeyInput input) {
        for (int i = 0; i < firstInactive; i++) {
            enemies.get(i).handleInput(input);
        }
    }

    /**
     * Updates active enemies, sorts the pool and spawns new enemies. The pool is sorted such that all active
     * enemies are in the first part of the vector pool.
     *
     * @param worldBounds a rectangle representing world boundaries
     * @param map a map of the world with which to check collision
     * @param obstacleManager an obstacle manager of the world with which to check collision
     * @param playerManager a player manager of the world with which to check collision
     * @param enemyPool an enemy pool of the world with which to check collision
     * @param projectileManager a projectile manager of the world with which to check collision
     * @param pickupsPool a pickups pool of the world with which to check collision
     * @param effectsPool an effects pool from which to play destroy animations
     * @param score a score object to add to
     * @see Entity
     * @see Map
     * @see ObstacleManager
     * @see PlayerManager
     * @see EnemyPool
     * @see ProjectileManager
     * @see PickupsPool
     * @see EffectsPool
     * @see Score
     */
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager,
                       PlayerManager playerManager, EnemyPool enemyPool,
                       ProjectileManager projectileManager, PickupsPool pickupsPool,
                       EffectsPool effectsPool, Score score) {

        for (int i = 0; i < firstInactive; i++) {
            enemies.get(i).update(worldBounds, map, obstacleManager, playerManager, enemyPool,
                                    projectileManager, pickupsPool, effectsPool, score);
        }

        // sort pool
        for (int i = firstInactive - 1; i >= 0; i--) {
            Tank enemy = enemies.get(i);

            if (enemy.destroyed()) {
                score.addScore(100 * scale * (enemy.getLevel() + 1));

                effectsPool.play(EffectsPool.EXPLOSION, (int) enemy.getX(), (int) enemy.getY());
                Game.LOCATOR.getAudioService().playSound(SoundID.BLOW_UP);

                firstInactive--;
                Collections.swap(enemies, i, firstInactive);
                enemy.repair();
            }
        }

        cooldown--;
        //spawn if not on cooldown
        if (cooldown <= 0) {
            spawnEnemy(playerManager);

            cooldown = nextCooldown();
        }
    }

    /**
     * Draws all enemies on a graphics context.
     *
     * @param graphics a graphics context on which to draw enemies
     */
    public void draw(Graphics graphics) {
        for (int i = 0; i < firstInactive; i++) {
            enemies.get(i).draw(graphics);
        }
    }

    /**
     * Checks collision for all enemies with an entity.
     *
     * @param entity an entity to check collision with
     * @return a flag that is true if collision with any of the enemies was detected
     * @see Entity
     */
    public boolean checkCollision(Entity entity) {
        for (int i = 0; i < firstInactive; i++) {
            if (enemies.get(i).checkCollision(entity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks collision for all enemies with a rectangle.
     *
     * @param rectangle a rectangle to check collision with
     * @return a flag that is true if collision with any of the enemies was detected
     */
    public boolean checkCollision(Rectangle rectangle) {
        for (int i = 0; i < firstInactive; i++) {
            if (enemies.get(i).checkCollision(rectangle)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks collision for all enemies with a projectile setting for destruction any enemy it collides with.
     *
     * @param projectile an entity to check collision with
     * @return a flag that is true if collision with any of the enemies was detected
     * @see Projectile
     */
    public boolean checkDestructiveCollision(Projectile projectile) {
        boolean collided = false;
        for (int i = 0; i < firstInactive; i++) {
            collided |= enemies.get(i).checkDestructiveCollision(projectile);
        }
        return collided;
    }

    /**
     * Obtain enemy coordinates.
     *
     * @return a vector of points of the enemy coordinates
     */
    public Vector<Point> getEnemiesPosition() {
        Vector<Point> enemiesPosition = new Vector<>(firstInactive);

        for (int i = 0; i < firstInactive; i++) {
            Tank enemy = enemies.get(i);
            enemiesPosition.add(new Point((int) enemy.getX(), (int) enemy.getY()));
        }

        return enemiesPosition;
    }

    /**
     * Obtain enemy levels.
     *
     * @return a vector of enemy levels
     */
    public Vector<Integer> getEnemiesLevel() {
        Vector<Integer> enemiesLevel = new Vector<>(firstInactive);

        for (int i = 0; i < firstInactive; i++) {
            Tank enemy = enemies.get(i);
            enemiesLevel.add(enemy.getLevel());
        }

        return enemiesLevel;
    }

    /**
     * Obtain enemy health points.
     *
     * @return a vector of enemy health points
     */
    public Vector<Integer> getEnemiesHp() {
        Vector<Integer> enemiesHp = new Vector<>(firstInactive);

        for (int i = 0; i < firstInactive; i++) {
            Tank enemy = enemies.get(i);
            enemiesHp.add(enemy.getHp());
        }

        return enemiesHp;
    }

    /**
     * Calculates the next random cooldown for enemy spawning.
     *
     * @return next cooldown for enemy spawning
     */
    private int nextCooldown() {
        return (random.nextInt(MAX_COOLDOWN) + MIN_DELAY) / scale;
    }

    /**
     * Spawns an enemy at a random location in the spawn area, unless the pool limit is reached or a valid
     * spawn point is not found after the maximum number of tries.
     *
     * @param playerManager a player manager to check collision on generated spawn location with
     */
    private void spawnEnemy(PlayerManager playerManager) {
        if (firstInactive == MAX_ENEMIES) {
            return;
        }

        int spawnY;
        boolean badSpawn;

        int tries = MAX_SPAWN_TRIES;

        do {
            spawnY = random.nextInt(MAX_SPAWN_HEIGHT);
            Rectangle bounds = new Rectangle(SPAWN_X, spawnY, Assets.TANK_HEIGHT, Assets.TANK_HEIGHT);

            badSpawn = playerManager.checkCollision(bounds)
                    || checkCollision(bounds);

            tries--;
        } while (badSpawn && tries > 0);

        if (tries <= 0) {
            return;
        }

        respawn(firstInactive, SPAWN_X, spawnY);
        firstInactive++;
    }

    /**
     * Respawns an enemy using a position in the pool at a given location.
     *
     * @param index index in the pool to use
     * @param x x coordinate of the respawn
     * @param y y coordinate of the respawn
     */
    private void respawn(int index, double x, double y) {
        Tank enemy = enemies.get(index);
        enemy.respawn(new AIInputComponent(), x, y);
    }
}
