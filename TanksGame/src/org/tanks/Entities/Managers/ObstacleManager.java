package org.tanks.Entities.Managers;

import org.tanks.Assets.Assets;
import org.tanks.Entities.Objects.Obstacles.*;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Map.Map;
import org.tanks.Pickups.Pickup;
import org.tanks.World;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

/**
 * The {@code ObstacleManager} class is used for managing {@code Obstacle} type objects. On restart it spawns
 * obstacles at random locations on the map.
 *
 * @see Obstacle
 * @author Beldiman Vladislav
 */
public class ObstacleManager {
    /**
     * minimum value for x spawn coordinate
     */
    private static final int MIN_SPAWN_X = 80;

    /**
     * maximum value for x spawn coordinate
     */
    private static final int MAX_SPAWN_X = 1150;

    /**
     * minimum value for y spawn coordinate
     */
    private static final int MIN_SPAWN_Y = 50;

    /**
     * maximum value for y spawn coordinate
     */
    private static final int MAX_SPAWN_Y = 700;

    /**
     * a random used for generating spawn coordinates
     */
    private static final Random random = new Random();

    /**
     * a vector of managed obstacles
     */
    private Vector<Obstacle> obstacles;

    /**
     * Restarts this creating new obstacles on a map.
     *
     * @param map a map to spawn obstacles on
     * @param noObstacles the number of obstacles to spawn
     * @see Map
     */
    public void restart(Map map, int noObstacles) {
        obstacles = new Vector<>(noObstacles);

        for (int i = 0; i < noObstacles; i++) {
            Point spawnPoint = getNextSpawn(map, i);
            ObstacleType obstacleType = ObstacleType.values()[random.nextInt(3)];
            Obstacle nextObstacle = getNextObstacle(spawnPoint.getX(), spawnPoint.getY(), obstacleType);
            obstacles.add(nextObstacle);
        }
    }

    /**
     * Loads obstacles from saved data.
     *
     * @param obstaclesCoord a vector of points of the coordinates of obstacles
     * @param obstaclesType a vector of the types of obstacles
     * @see ObstacleType
     */
    public void load(Vector<Point> obstaclesCoord, Vector<ObstacleType> obstaclesType) {
        int noObstacles = obstaclesCoord.size();
        obstacles = new Vector<>(noObstacles);

        for (int i = 0; i < noObstacles; i++) {
            Point point = obstaclesCoord.get(i);
            Obstacle obstacle = getNextObstacle(point.getX(), point.getY(), obstaclesType.get(i));
            obstacles.add(obstacle);
        }
    }

    /**
     * Draws managed obstacles on a graphics context.
     *
     * @param graphics a graphics context to draw obstacles on
     */
    public void draw(Graphics graphics) {
        for (Obstacle obstacle: obstacles) {
            obstacle.draw(graphics);
        }
    }

    /**
     * Checks collision for all obstacles with a rectangle.
     *
     * @param rectangle a rectangle to check collision with
     * @return a flag that is true if collision with any of the obstacles was detected
     */

    public boolean checkCollision(Rectangle rectangle) {
        for (Obstacle obstacle: obstacles) {
            if (obstacle.checkCollision(rectangle)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Obtains obstacle coordinates.
     *
     * @return a vector of points of the coordinates of obstacles
     */
    public Vector<Point> getObstaclesPosition() {
        Vector<Point> obstaclesPosition = new Vector<>(obstacles.size());

        for (Obstacle obstacle: obstacles) {
            obstaclesPosition.add(new Point((int) obstacle.getX(), (int) obstacle.getY()));
        }

        return obstaclesPosition;
    }

    /**
     * Obtains obstacle types.
     *
     * @return a vector of obstacle types
     * @see ObstacleType
     */
    public Vector<ObstacleType> getObstaclesType() {
        Vector<ObstacleType> obstaclesType = new Vector<>(obstacles.size());

        for (Obstacle obstacle: obstacles) {
            obstaclesType.add(obstacle.getObstacleType());
        }

        return obstaclesType;
    }

    /**
     * Creates a new obstacle of a given type.
     *
     * @param x x coordinate of the obstacle
     * @param y y coordinate of the obstacle
     * @param obstacleType obstacle type
     * @return the new object of type obstacle
     */
    private Obstacle getNextObstacle(double x, double y, ObstacleType obstacleType) {
        return switch (obstacleType) {
            case HEDGEHOG -> new Hedgehog(x, y);
            case TREE -> new Tree(x, y);
            case LOGS -> new Logs(x, y);
        };
    }

    /**
     * Generates a new x coordinate for obstacle spawning.
     *
     * @return the generated x coordinate
     */
    private int getNextX() {
        return random.nextInt(MAX_SPAWN_X - MIN_SPAWN_X) + MIN_SPAWN_X;
    }

    /**
     * Generates a new y coordinate for obstacle spawning.
     *
     * @return the generated y coordinate
     */
    private int getNextY() {
        return random.nextInt(MAX_SPAWN_Y - MIN_SPAWN_Y) + MIN_SPAWN_Y;
    }

    /**
     * Generates a new spawn point that does not intersect with already created obstacles.
     *
     * @param map a map to get a spawn location on
     * @param lastSpawned the last created obstacle
     * @return a point of (x, y) coordinates of the a new spawn
     * @see Map
     */
    private Point getNextSpawn(Map map, int lastSpawned) {
        int x;
        int y;
        int width = Assets.TILE_WIDTH;
        int height = Assets.TILE_HEIGHT;

        boolean badSpawn;

        do {
            x = getNextX();
            y = getNextY();
            Rectangle bounds = new Rectangle(x, y, width, height);

            badSpawn = map.checkCollision(bounds);
            // check internal collision only with obstacles already created
            for (int i = 0; i < lastSpawned; i++) {
                if (obstacles.get(i).checkCollision(bounds)) {
                    badSpawn = true;
                    break;
                }
            }

        } while (badSpawn);

        return new Point(x, y);
    }
}
