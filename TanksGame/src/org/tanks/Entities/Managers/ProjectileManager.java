package org.tanks.Entities.Managers;

import org.tanks.AudioService.SoundID;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Entities.Objects.Projectiles.ProjectileType;
import org.tanks.Entities.Objects.Projectiles.Rocket;
import org.tanks.Entities.Objects.Projectiles.Shell;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;
import java.util.Vector;

/**
 * The {@code ProjectileManager} class is used as a manager of the projecties.
 *
 * @see Projectile
 */
public class ProjectileManager {
    /**
     * a vector of shot projectiles
     */
    protected Vector<Projectile> shotProjectiles;

    /**
     * Restarts this.
     */
    public void restart() {
        shotProjectiles = new Vector<>(32);
    }

    /**
     * Load projectiles from saved data.
     *
     * @param projectilesCoord a vector of points of the coordinates of projectiles
     * @param projectilesDirection a vector of projectile flying direction
     * @param projectilesType a vector of projectile types
     * @param projectilesAllegiance a vector of projectile allegiances
     * @see Direction
     * @see ProjectileType
     * @see Allegiance
     */
    public void load(Vector<Point> projectilesCoord, Vector<Direction> projectilesDirection,
                     Vector<ProjectileType> projectilesType,
                     Vector<Allegiance> projectilesAllegiance) {

        shotProjectiles = new Vector<>(32);

        for (int i = 0; i < projectilesCoord.size(); i++) {
            Allegiance allegiance = projectilesAllegiance.get(i);
            Point point = projectilesCoord.get(i);
            double x = point.getX();
            double y = point.getY();
            Direction direction = projectilesDirection.get(i);

            Projectile projectile = switch (projectilesType.get(i)) {
                case SHELL -> new Shell(allegiance, x, y, direction);
                case ROCKET -> new Rocket(allegiance, x, y, direction);
            };

            shotProjectiles.add(projectile);
        }
    }

    /**
     * Updates projectiles.
     *
     * @param worldBounds a rectangle representing world boundaries
     * @param map a map of the world with which to check collision
     * @param obstacleManager an obstacle manager of the world with which to check collision
     * @param playerManager a player manager of the world with which to check collision
     * @param enemyPool an enemy pool of the world with which to check collision
     * @param projectileManager a projectile manager of the world with which to check collision
     * @param pickupsPool a pickups pool of the world with which to check collision
     * @param effectsPool an effects pool from which to play destroy animations
     * @param score a score object to add to
     * @see Entity
     * @see Map
     * @see ObstacleManager
     * @see PlayerManager
     * @see EnemyPool
     * @see ProjectileManager
     * @see PickupsPool
     * @see EffectsPool
     * @see Score
     */
    public void update(Rectangle worldBounds, Map map, ObstacleManager obstacleManager,
                       PlayerManager playerManager, EnemyPool enemyPool,
                       ProjectileManager projectileManager, PickupsPool pickupsPool,
                       EffectsPool effectsPool, Score score) {

        for (Projectile projectile: shotProjectiles) {
            projectile.update(worldBounds, map, obstacleManager, playerManager, enemyPool,
                                projectileManager, pickupsPool, effectsPool, score);
        }

        for (int i = shotProjectiles.size() - 1; i >= 0; i--) {
            Projectile projectile = shotProjectiles.get(i);

            if (projectile.destroyed()) {
                effectsPool.play(EffectsPool.FLASH, (int) projectile.getX(), (int) projectile.getY());
                Game.LOCATOR.getAudioService().playSound(SoundID.BLOW_UP);
                shotProjectiles.remove(i);
            }
        }
    }

    /**
     * Draws projectiles on a graphics context.
     *
     * @param graphics a graphics context on which to draw projectiles on
     */
    public void draw(Graphics graphics) {
        for (Projectile projectile: shotProjectiles) {
            projectile.draw(graphics);
        }
    }

    /**
     * Adds a projectile to the vector of projectiles.
     *
     * @param projectile projectile to add
     * @see Projectile
     */
    public void addProjectile(Projectile projectile) {
        shotProjectiles.add(projectile);
    }

    /**
     * Checks collision for all projectiles with a projectile and sets any projectile that collides with it for
     * destruction.
     *
     * @param checkedProjectile a projectile to check collision with
     * @return a flag that is true if there is any collision with this' projectiles
     * @see Projectile
     */
    public boolean checkDestructiveCollision(Projectile checkedProjectile) {
        boolean collided = false;

        for (Projectile projectile: shotProjectiles) {
            collided |= projectile.checkDestructiveCollision(checkedProjectile);
        }

        return collided;
    }

    /**
     * Obtains projectile coordinates.
     *
     * @return a vector of points of the coordinates of the player.
     */
    public Vector<Point> getProjectilesPosition() {
        Vector<Point> projectilesPosition = new Vector<>(20);

        for (Projectile projectile: shotProjectiles) {
            projectilesPosition.add(new Point((int) projectile.getX(), (int) projectile.getY()));
        }

        return projectilesPosition;
    }

    /**
     * Obtains projectile directions.
     *
     * @return a vector of projectiles directions
     * @see Direction
     */
    public Vector<Direction> getProjectilesDirection() {
        Vector<Direction> projectilesDirection = new Vector<>(20);

        for (Projectile projectile: shotProjectiles) {
            projectilesDirection.add(projectile.getDirection());
        }

        return projectilesDirection;
    }

    /**
     * Obtains projectile types.
     *
     * @return a vector of projectile types
     * @see ProjectileType
     */
    public Vector<ProjectileType> getProjectilesType() {
        Vector<ProjectileType> projectilesType = new Vector<>(20);

        for (Projectile projectile: shotProjectiles) {
            projectilesType.add(projectile.getProjectileType());
        }

        return projectilesType;
    }

    /**
     * Obtains projectile allegiances.
     *
     * @return a vector of projectile allegiances
     * @see Allegiance
     */
    public Vector<Allegiance> getProjectilesAllegiance() {
        Vector<Allegiance> projectilesAllegiance = new Vector<>(20);

        for (Projectile projectile: shotProjectiles) {
            projectilesAllegiance.add(projectile.getAllegiance());
        }

        return projectilesAllegiance;
    }
}
