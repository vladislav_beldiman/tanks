package org.tanks.Entities;

/**
 * The {@code Allegiance} enum specifies the available allegiances for entities.
 *
 * @see Entity
 * @author Beldiman Vladislav
 */
public enum Allegiance {
    /**
     * allegiance to player
     */
    Player,

    /**
     * allegiance to enemy
     */
    Enemy,

    /**
     * allegiance to no one
     */
    Neutral
}
