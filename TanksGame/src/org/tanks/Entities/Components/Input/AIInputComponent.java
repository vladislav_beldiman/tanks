package org.tanks.Entities.Components.Input;

import org.tanks.Entities.Entity;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Objects.TankStates.GunStates.GunState;
import org.tanks.Entities.Objects.TankStates.GunStates.ReadyState;
import org.tanks.Entities.Objects.TankStates.MoveStates.DirectionState;
import org.tanks.Input.KeyInput;

import java.util.Random;

/**
 * The {@code AIInputComponent} class provides an implementation of the {@code InputComponent} interface for
 * an AI controlled {@code Tank} entity. It generates a random direction for a tank and makes it shoot
 * whenever it is ready to do so.
 *
 * @see InputComponent
 * @see Tank
 * @see Entity
 * @author Beldiman Vladislav
 */
public class AIInputComponent implements InputComponent {
    /**
     * standard cooldown for direction input generation
     */
    private static final int INPUT_COOLDOWN = 60;

    /**
     * random instance used for direction input generation
     */
    private static final Random RANDOM = new Random();

    /**
     * current cooldown for direction input generation
     */
    private int cooldown = INPUT_COOLDOWN;

    @Override
    public void handleInput(Entity entity, KeyInput input) {
        // if the entity is not of type Tank something is very wrong
        Tank tank = (Tank) entity;
        DirectionState directionState = tank.getDirectionState();
        Direction currentDirection = directionState.getDirection();

        // key input replacement
        boolean[] keyState = new boolean[256];

        cooldown--;

        if (cooldown < 0) {
            cooldown = INPUT_COOLDOWN;
            int nextDirection = RANDOM.nextInt(100);

            // keep going in the current direction with a 92 % chance
            // each other direction (including not moving) has a 2 % chance
            if (nextDirection < 90) {
                keyState[currentDirection.getKey()] = true;
            } else if (nextDirection < 92) {
                keyState[KeyInput.UP] = true;
            } else if (nextDirection < 94) {
                keyState[KeyInput.DOWN] = true;
            } else if (nextDirection < 96) {
                keyState[KeyInput.LEFT] = true;
            } else if (nextDirection < 98) {
                keyState[KeyInput.RIGHT] = true;
            }
            // else do not move
        }
        else {
            // if direction input generation is on cooldown keep going in the current direction
            keyState[currentDirection.getKey()] = true;
        }

        GunState gunState = tank.getGunState();

        // shoot when ready
        if (gunState.getClass() == ReadyState.class) {
            keyState[KeyInput.SPACE] = true;
        }

        // input replacement
        KeyInput aiInput = new KeyInput(keyState);

        DirectionState nextDirectionState = directionState.handleInput(tank, aiInput);
        tank.setDirectionState(nextDirectionState);

        if (aiInput.spacePressed()) {
            GunState nextGunState = gunState.shootFrom(tank);
            tank.setGunState(nextGunState);
        }
    }
}
