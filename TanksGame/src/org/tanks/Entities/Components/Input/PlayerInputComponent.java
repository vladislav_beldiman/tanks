package org.tanks.Entities.Components.Input;

import org.tanks.Entities.Entity;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Entities.Objects.TankStates.GunStates.GunState;
import org.tanks.Entities.Objects.TankStates.MoveStates.DirectionState;
import org.tanks.Input.KeyInput;

/**
 * The {@code PlayerInputComponent} class is an implementation of the {@code InputComponent} interface responsible
 * for handling player input.
 *
 * @see InputComponent
 * @see KeyInput
 * @see Tank
 * @author Beldiman Vladislav
 */
public class PlayerInputComponent implements InputComponent {
    @Override
    public void handleInput(Entity entity, KeyInput input) {
        // if the entity is not of type Tank something is very wrong
        Tank tank = (Tank) entity;
        DirectionState nextDirectionState = tank.getDirectionState().handleInput(tank, input);
        tank.setDirectionState(nextDirectionState);

        if (input.spacePressed()) {
            GunState nextGunState = tank.getGunState().shootFrom(tank);
            tank.setGunState(nextGunState);
        }
    }
}
