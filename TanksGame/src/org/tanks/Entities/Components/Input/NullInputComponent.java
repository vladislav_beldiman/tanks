package org.tanks.Entities.Components.Input;

import org.tanks.Entities.Entity;
import org.tanks.Input.KeyInput;

/**
 * The {@code NullInputComponent} class is a "do nothing" implementation of the {@code InputComponent} interface.
 *
 * @see InputComponent
 * @author Beldiman Vladislav
 */
public class NullInputComponent implements InputComponent {
    @Override
    public void handleInput(Entity entity, KeyInput input) {
        /* do nothing */
    }
}
