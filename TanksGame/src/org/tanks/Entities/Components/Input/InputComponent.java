package org.tanks.Entities.Components.Input;

import org.tanks.Entities.Entity;
import org.tanks.Input.KeyInput;

/**
 * The {@code InputComponent} is an interface for components responsible for handling input for an entity.
 *
 * @see Entity
 * @author Beldiman Vladislav
 */
public interface InputComponent {
    /**
     * Handles input for an entity.
     *
     * @param entity an entity
     * @param input the key input for the entity
     * @see Entity
     * @see KeyInput
     */
    void handleInput(Entity entity, KeyInput input);
}
