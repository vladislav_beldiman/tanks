package org.tanks.Entities.Components.Input;

import org.tanks.Input.KeyInput;

/**
 * The {@code Direction} is specifies the 4 available directions and a no move (still) state. It provides
 * unit velocities for x and y axis and the {@code KeyInput} key corresponding to a direction.
 *
 * @see KeyInput
 * @author Beldiman Vladislav
 */
public enum Direction {
    /**
     * up direction
     */
    UP,
    /**
     * down direction
     */
    DOWN,
    /**
     * left direction
     */
    LEFT,
    /**
     * right direction
     */
    RIGHT,
    /**
     * no move state
     */
    STILL;

    /**
     * Obtains the unit velocity on the x axis.
     *
     * @return the unit velocity on the x axis
     */
    public double getUnitVelX() {
        return switch (this) {
            case LEFT -> -1;
            case RIGHT -> 1;
            /* intentionally left STILL, UP and DOWN for default return */
            default -> 0;
        };
    }

    /**
     * Obtains the unit velocity on the y axis.
     *
     * @return the unit velocity on the y axis
     */
    public double getUnitVelY() {
        return switch (this) {
            case UP -> -1;
            case DOWN -> 1;
            /* intentionally left STILL, LEFT and RIGHT for default return */
            default -> 0;
        };
    }

    /**
     * Obtains the key corresponding to moving in a given direction used in {@code KeyInput}.
     *
     * @return a number representing the {@code KeyInput} key for the direction
     * @see KeyInput
     */
    public int getKey() {
        return switch (this) {
            case UP -> KeyInput.UP;
            case DOWN -> KeyInput.DOWN;
            case LEFT -> KeyInput.LEFT;
            case RIGHT -> KeyInput.RIGHT;
            /* this should not ever be called on STILL */
            default -> -1;
        };
    }
}
