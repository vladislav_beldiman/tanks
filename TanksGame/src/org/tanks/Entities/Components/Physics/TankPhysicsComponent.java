package org.tanks.Entities.Components.Physics;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.MovableEntity;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Entities.Objects.Tank;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code TankPhysicsComponent} class provides an implementation for the {@code PhysicsComponent} interface
 * for {@code Tank} objects.
 *
 * @see PhysicsComponent
 * @see Tank
 * @author Beldiman Vladislav
 */
public class TankPhysicsComponent implements PhysicsComponent {
    /**
     * x coordinate before move. Used for undoing move on collision
     */
    private double oldX;

    /**
     * y coordinate before move. Used for undoing move on collision
     */
    private double oldY;

    /**
     * a flag set if move should be undone during collision solving
     */
    private boolean undoMove;

    /**
     * a flag set if the tank should be leveled up during collision solving
     */
    private boolean levelUp;

    @Override
    public void move(Entity entity) {
        // if the entity is not of type Tank something is very wrong
        Tank tank = (Tank) entity; 

        oldX = entity.getX();
        oldY = entity.getY();

        double velScale = tank.getVelScale();
        double velX = tank.getUnitVelX() * velScale;
        double velY = tank.getUnitVelY() * velScale;

        tank.setX(oldX + velX);
        tank.setY(oldY + velY);
    }

    @Override
    public void checkCollision(Entity entity, Rectangle worldBounds, Map map,
                               ObstacleManager obstacleManager, PlayerManager playerManager,
                               EnemyPool enemyPool, ProjectileManager projectileManager,
                               PickupsPool pickupsPool) {
        undoMove = false;
        Rectangle bounds = entity.getBounds();

        if (!worldBounds.contains(bounds)
            || map.checkCollision(bounds)
            || obstacleManager.checkCollision(bounds)
            || playerManager.checkCollision(entity)
            || enemyPool.checkCollision(entity)) {

            undoMove = true;
        }

        levelUp = pickupsPool.checkEatingCollision(entity);
    }

    @Override
    public void solveCollision(Entity entity, EffectsPool effectsPool, Score score) {
        if (undoMove) {
            entity.setX(oldX);
            entity.setY(oldY);
        }

        if (levelUp) {
            // if the entity is not of type Tank something is very wrong
            ((Tank) entity).levelUp(effectsPool, score);
        }
    }

    @Override
    public boolean checkCollision(Entity entity1, Entity entity2) {
        return entity1 != entity2 && entity1.getBounds().intersects(entity2.getBounds());
    }

    @Override
    public boolean checkCollision(Entity entity, Rectangle rectangle) {
        return entity.getBounds().intersects(rectangle);
    }

    @Override
    public boolean checkDestructiveCollision(Entity entity, Projectile projectile) {
        boolean collided = projectile.getAllegiance() != entity.getAllegiance() && entity.checkCollision(projectile);

        if (collided) {
            // if entity is not a movable entity something is very wrong
            ((MovableEntity) entity).damage(projectile.getDamage());
        }

        return collided;
    }
}
