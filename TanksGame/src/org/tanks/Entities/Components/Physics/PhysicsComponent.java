package org.tanks.Entities.Components.Physics;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code PhysicsComponent} is an interface for components responsible for handling entity physics.
 *
 * @see Entity
 * @author Beldiman Vladislav
 */
public interface PhysicsComponent {
    /**
     * Moves entity to a new location.
     *
     * @param entity an entity to be moved
     */
    void move(Entity entity);

    /**
     * Checks for possible collision with other entities, map tiles, and world boundaries.
     *
     * @param entity an entity for which to check collision
     * @param worldBounds a rectangle representing the bounds of the world with which to check collision
     * @param map a map of the world with which to check collision
     * @param obstacleManager an obstacle manager of the world with which to check collision
     * @param playerManager a player manager of the world with which to check collision
     * @param enemyPool an enemy pool of the world with which to check collision
     * @param projectileManager a projectile manager of the world with which to check collision
     * @param pickupsPool a pickups pool of the world with which to check collision
     * @see Entity
     * @see Map
     * @see ObstacleManager
     * @see PlayerManager
     * @see EnemyPool
     * @see ProjectileManager
     * @see PickupsPool
     */
    void checkCollision(Entity entity, Rectangle worldBounds, Map map,
                               ObstacleManager obstacleManager, PlayerManager playerManager,
                               EnemyPool enemyPool, ProjectileManager projectileManager,
                               PickupsPool pickupsPool);

    /**
     * Solves any collision with other entities, map tiles, or world boundaries.
     *
     * @param entity an entity for which to solve collision
     * @param effectsPool an effects pool from which to play entity destruction animations
     * @param score a score object to add to
     * @see Entity
     * @see EffectsPool
     * @see Score
     */
    void solveCollision(Entity entity, EffectsPool effectsPool, Score score);

    /**
     * Checks collision this with an entity.
     *
     * @param entity1 an entity with which to check collision with
     * @param entity2 another entity with which to check collision with
     * @return a flag that is true, if the entity bounds do not intersect.
     */
    boolean checkCollision(Entity entity1, Entity entity2);

    /**
     * Checks collision this with a rectangle.
     *
     * @param entity an entity with which to check collision with
     * @param rectangle entity with which to check collision with this
     * @return a flag that is true, if the rectangle do not intersect.
     */
    boolean checkCollision(Entity entity, Rectangle rectangle);

    /**
     * Checks for collision of an entity with a projectile. Collision is ignored, if the entity and the
     * projectile have the same allegiance. In the case of collision the entity is damaged.
     *
     * @param entity an entity for which to solve collision
     * @param projectile a projectile to check collision with
     * @return a flag that is true, if a collision occurred
     */
    boolean checkDestructiveCollision(Entity entity, Projectile projectile);
}
