package org.tanks.Entities.Components.Physics;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;

import java.awt.*;

public class ImmutablePhysicsComponent implements PhysicsComponent {
    @Override
    public void move(Entity entity) {
        /* do nothing */
    }

    @Override
    public void checkCollision(Entity entity, Rectangle worldBounds, Map map,
                               ObstacleManager obstacleManager, PlayerManager playerManager,
                               EnemyPool enemyPool, ProjectileManager projectileManager,
                               PickupsPool pickupsPool) {
        /* do nothing */
    }

    @Override
    public void solveCollision(Entity entity, EffectsPool effectsPool, Score score) {
        /* do nothing */
    }

    @Override
    public boolean checkCollision(Entity entity1, Entity entity2) {
        return entity1 != entity2 && entity1.getBounds().intersects(entity2.getBounds());
    }

    @Override
    public boolean checkCollision(Entity entity, Rectangle rectangle) {
        return entity.getBounds().intersects(rectangle);
    }

    @Override
    public boolean checkDestructiveCollision(Entity entity, Projectile projectile) {
        // can not destroy immutable entity
        return projectile.getAllegiance() != entity.getAllegiance() && entity.checkCollision(projectile);
    }
}
