package org.tanks.Entities.Components.Physics;

import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.MovableEntity;
import org.tanks.Entities.Objects.Projectiles.Projectile;
import org.tanks.Map.Map;
import org.tanks.Pickups.PickupsPool;
import org.tanks.Score;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code ProjectilePhysicsComponent} class provides an implementation of the {@code PhysicsComponent}
 * interface for objects of type {@code Projectile}.
 *
 * @see PhysicsComponent
 * @see Projectile
 * @author Beldiman Vladislav
 */
public class ProjectilePhysicsComponent implements PhysicsComponent {
    /**
     * a flag set if projectile should be destroyed during collision solving
     */
    private boolean destroy;

    @Override
    public void move(Entity entity) {
        // If entity is not of type projectile something is very wrong
        Projectile projectile = (Projectile) entity;

        double x = projectile.getX();
        double y = projectile.getY();
        double velScale = projectile.getVelScale();
        double velX = projectile.getUnitVelX() * velScale;
        double velY = projectile.getUnitVelY() * velScale;

        projectile.setX(x + velX);
        projectile.setY(y + velY);
    }

    @Override
    public void checkCollision(Entity entity, Rectangle worldBounds, Map map,
                               ObstacleManager obstacleManager, PlayerManager playerManager,
                               EnemyPool enemyPool, ProjectileManager projectileManager,
                               PickupsPool pickupsPool) {
        // If entity is not of type projectile something is very wrong
        Projectile projectile = (Projectile) entity;
        Rectangle bounds = projectile.getBounds();

        destroy = !worldBounds.contains(bounds);
        destroy |= obstacleManager.checkCollision(bounds);
        destroy |= playerManager.checkDestructiveCollision(projectile);
        destroy |= enemyPool.checkDestructiveCollision(projectile);
        destroy |= projectileManager.checkDestructiveCollision(projectile);
    }

    @Override
    public void solveCollision(Entity entity, EffectsPool effectsPool, Score score) {
        // If entity is not of type projectile something is very wrong
        Projectile projectile = (Projectile) entity;

        if (destroy) {
            projectile.damage(1);
        }
    }

    @Override
    public boolean checkCollision(Entity entity1, Entity entity2) {
        return entity1 != entity2 && entity1.getBounds().intersects(entity2.getBounds());
    }

    @Override
    public boolean checkCollision(Entity entity, Rectangle rectangle) {
        return entity.getBounds().intersects(rectangle);
    }

    @Override
    public boolean checkDestructiveCollision(Entity entity, Projectile projectile) {
        boolean collided = projectile.getAllegiance() != entity.getAllegiance() && entity.checkCollision(projectile);

        if (collided) {
            // if entity is not a movable entity something is very wrong
            ((MovableEntity) entity).damage(projectile.getDamage());
        }

        return collided;
    }
}
