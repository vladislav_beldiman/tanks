package org.tanks.Entities.Components.Graphics;

import org.tanks.Entities.Entity;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code GraphicsComponent} class is a graphics component of an entity responsible for drawing it. It
 * does so on a graphics context.
 *
 * @see Entity
 * @author Beldiman Vladislav
 */
public class GraphicsComponent {
    /**
     * Draws an entity on a graphics context.
     *
     * @param entity entity to be drawn
     * @param graphics graphics context on with the entity is to be drawn
     * @see Entity
     */
    public void draw(Entity entity, Graphics graphics) {
        BufferedImage texture = entity.getTexture();
        int x = (int) entity.getX();
        int y = (int) entity.getY();

        graphics.drawImage(texture, x, y, null);

        // draw hitbox used only for debugging purpose
//        graphics.setColor(Color.red);
//        graphics.drawRect(x, y, entity.getWidth(), entity.getHeight());
    }
}
