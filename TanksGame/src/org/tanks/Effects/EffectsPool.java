package org.tanks.Effects;

import org.tanks.Assets.Assets;

import java.awt.*;
import java.util.Collections;
import java.util.Vector;

/**
 * The {@code EffectsPool} class is used as an object pool for animations of class {@code Effect}. It holds
 * all playable animations, that is {@code EffectType} type objects, as flyweights.
 *
 * @see Effect
 * @see EffectType
 * @author Beldiman Vladislav
 */
public class EffectsPool {
    /**
     * flash animation flyweight
     */
    public static final EffectType FLASH = new EffectType(Assets.FLASH, 1);

    /**
     * explosion animation flyweight
     */
    public static final EffectType EXPLOSION = new EffectType(Assets.FLASH, 5);

    /**
     * level up animation flyweight
     */
    public static final EffectType LEVEL_UP = new EffectType(Assets.LEVEL_UP, 1);

    /**
     * the maximum number of playable animations at the same time
     */
    private static final int MAX_EFFECTS = 32;

    /**
     * the first inactive effect in the pool
     */
    private int firstInactive;

    /**
     * the vector pool of effects
     */
    private Vector<Effect> effects;

    /**
     * Restarts the effect pool. This is done by creating another vector of effects and filling it with
     * inactive effects.
     *
     * @see Effect
     */
    public void restart() {
        firstInactive = 0;
        effects = new Vector<>(MAX_EFFECTS);

        for (int i = 0; i < MAX_EFFECTS; i++) {
            effects.add(new Effect());
        }
    }

    /**
     * Updates all active animations.
     */
    public void update() {
        for (int i = 0; i < firstInactive; i++) {
            effects.get(i).update();
        }

        sortPool();
    }

    /**
     * Draws all active animations on given graphics context.
     *
     * @param graphics graphics context on which animations will be drawn
     */
    public void draw(Graphics graphics) {
        for (int i = 0; i < firstInactive; i++) {
            effects.get(i).draw(graphics);
        }
    }

    /**
     * Starts playing a new animation. It is played of a given type at a given position. If all objects in the
     * pool are busy playing animations, the first one in the vector is replaced. (Not necessarily the first one
     * that started playing.)
     *
     * @param effectType the effect type type object of the new animation
     * @param x the x coordinate of the animation
     * @param y the y coordinate of the animation
     * @see EffectType
     */
    public void play(EffectType effectType, int x, int y) {
        if (firstInactive == MAX_EFFECTS) {
            firstInactive--;
            Collections.swap(effects, 0, firstInactive);
        }

        Effect nextPlayed = effects.get(firstInactive);
        nextPlayed.play(effectType, x, y);
        firstInactive++;
    }

    /**
     * Sorts the pool such that active effects are in the first part of the vector pool. Inactive effects in the
     * first part are swapped with the current last active effect.
     */
    private void sortPool() {
        for (int i = firstInactive - 1; i >= 0; i--) {
            Effect effect = effects.get(i);

            if (effect.dead()) {
                firstInactive--;
                Collections.swap(effects, i, firstInactive);
            }
        }
    }
}
