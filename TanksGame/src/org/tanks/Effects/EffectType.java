package org.tanks.Effects;

import java.awt.image.BufferedImage;

/**
 * {@code EffectType} instances are type objects for objects of class {@code Effect}. They are used to store
 * the frames of the animation for a specific effect and its scale.
 *
 * @see Effect
 * @author Beldiman Vladislav
 */
public class EffectType {
    /**
     * an array of buffered images representing the frames of the animation
     */
    private final BufferedImage[] TEXTURE_ARRAY;

    /**
     * the scale of the animation
     */
    private final int SCALE;

    /**
     * Sole constructor
     *
     * @param TEXTURE_ARRAY an array of buffered images
     * @param SCALE the scale of the animation
     */
    public EffectType(BufferedImage[] TEXTURE_ARRAY, int SCALE) {
        this.TEXTURE_ARRAY = TEXTURE_ARRAY;
        this.SCALE = SCALE;
    }

    /**
     * Obtains the textures of the frames.
     *
     * @return an array of buffered images
     */
    public BufferedImage[] getTextures() {
        return TEXTURE_ARRAY;
    }

    /**
     * Obtains the scale of the animation.
     *
     * @return the scale of the animation
     */
    public int getScale() {
        return SCALE;
    }
}
