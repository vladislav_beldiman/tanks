package org.tanks.Effects;

import org.tanks.Assets.SpriteSheet;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Effect} class is used to play an animation. The animation is given its type object of class
 * {@code EffectType}. The animations dimensions are scalable.
 *
 * @see EffectType
 * @author Beldiman Vladislav
 */
public class Effect {
    /**
     * an effect type that is the type object of this
     *
     * @see EffectType
     */
    private EffectType effectType;

    /**
     * the x coordinate of the animation
     * */
    private int x;

    /**
     * the y coordinate of the animation
     */
    private int y;

    /**
     * the standard duration of an animation given in update cycles
     */
    public static final int ANIMATION_DURATION = 19;

    /**
     * the number of frames an animation has
     */
    protected static final int FRAMES = 5;

    /**
     * the amount of update cycles left for the animation
     */
    protected int lifeTime;

    /**
     * Updates the life time of the animation.
     */
    public void update() {
        if (lifeTime > 0) {
            lifeTime--;
        }
    }

    /**
     * Draws the animation on given graphics context.
     *
     * @param graphics a graphics context on which to draw the animation
     */
    public void draw(Graphics graphics) {
        if (lifeTime > 0) {
            BufferedImage texture = effectType.getTextures()[lifeTime / FRAMES];
            int scaledWidth = SpriteSheet.ASSET_WIDTH * effectType.getScale();
            int scaledHeight = SpriteSheet.ASSET_HEIGHT * effectType.getScale();

            graphics.drawImage(texture, x, y, scaledWidth, scaledHeight, null);
        }
    }

    /**
     * Reuses this to play an animation of another type object at another location. Resets the life time.
     *
     * @param effectType an effect type type object holding animation data
     * @param x an x coordinate for the animation
     * @param y an y coordinate for the animation
     */
    public void play(EffectType effectType, double x, double y) {
        this.effectType = effectType;
        this.x = (int) x;
        this.y = (int) y;
        this.lifeTime = ANIMATION_DURATION;
    }

    /**
     * Checks to see if the animation is not playing.
     *
     * @return true if animation is not currently playing
     */
    public boolean dead() {
        return lifeTime <= 0;
    }

    /**
     * Obtains the x coordinate of the animation.
     *
     * @return the x coordinate of the animation
     */
    public int getX() {
        return x;
    }

    /**
     * Obtains the y coordinate of the animation.
     *
     * @return the y coordinate of the animation
     */
    public int getY() {
        return y;
    }
}
