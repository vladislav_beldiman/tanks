package org.tanks;

/**
 * The {@code Score} encapsulates the current score in the game.
 *
 * @author Beldiman Vladislav
 */
public class Score {
    /**
     * the value of the current score
     */
    private int score;

    /**
     * Resets current score.
     */
    public void resetScore() { score = 0; }

    /**
     * Obtains the score value
     *
     * @return the score value
     */
    public int getScore() { return score; }

    /**
     * Adds an amount to the score
     *
     * @param amount the amount of score to add
     */
    public void addScore(int amount) { score += amount; }
}
