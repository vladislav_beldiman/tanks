package org.tanks.Map.Tiles;

import org.tanks.Assets.Assets;

/**
 * The {@code GrassTile} class is an extension of the {@code Tile} class representing a dirt tile.
 *
 * @see Tile
 * @author Beldiman Vladislav
 */
public class GrassTile extends Tile {
    /**
     * Sole contructor.
     */
    public GrassTile() {
        super(Assets.GRASS, true);
    }

    @Override
    public TileType getType() {
        return TileType.GRASS_TILE;
    }
}
