package org.tanks.Map.Tiles;

import org.tanks.Assets.Assets;

/**
 * The {@code WaterTile} class is an extension of the {@code Tile} class representing a dirt tile.
 *
 * @see Tile
 * @author Beldiman Vladislav
 */
public class WaterTile extends Tile {
    /**
     * Sole contructor.
     */
    public WaterTile() {
        super(Assets.WATER, false);
    }

    @Override
    public TileType getType() {
        return TileType.WATER_TILE;
    }
}