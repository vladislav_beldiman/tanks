package org.tanks.Map.Tiles;

import org.tanks.Assets.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Tile} class is an abstract class for flyweight map tiles.
 *
 * @see org.tanks.Map.Map
 * @author Beldiman Vladislav
 */
public abstract class Tile {
    /**
     * a buffered image representing this' texture
     */
    private final BufferedImage texture;

    /**
     * a flag that prevents moving to this, if set
     */
    private final boolean isPassable;

    /**
     * Sole constructor
     *
     * @param texture a buffered image of the texture
     * @param isPassable a flag that prevents moving to this, if set
     */
    public Tile(BufferedImage texture, boolean isPassable) {
        this.texture = texture;
        this.isPassable = isPassable;
    }

    /**
     * Draws tile on graphics context from coordinates (x, y)
     *
     * @param graphics a graphics context
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public void draw(Graphics graphics, int x, int y) {
        int scaledX = x * Assets.TILE_WIDTH;
        int scaledY = y * Assets.TILE_HEIGHT;
        graphics.drawImage(texture, scaledX, scaledY, null);
    }

    public boolean isPassable() {
        return isPassable;
    }

    /**
     * Obtains tile type.
     *
     * @return tile type
     */
    public abstract TileType getType();
}
