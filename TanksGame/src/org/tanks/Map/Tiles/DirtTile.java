package org.tanks.Map.Tiles;

import org.tanks.Assets.Assets;

/**
 * The {@code DirtTile} class is an extension of the {@code Tile} class representing a dirt tile.
 *
 * @see Tile
 * @author Beldiman Vladislav
 */
public class DirtTile extends Tile {
    /**
     * Sole contructor.
     */
    public DirtTile() {
        super(Assets.DIRT, true);
    }


    @Override
    public TileType getType() {
        return TileType.DIRT_TILE;
    }
}
