package org.tanks.Map.Tiles;

/**
 * The {@code TileType} enum specifies available flyweight tiles.
 *
 * @see Tile
 * @author Beldiman Vladislav
 */
public enum TileType {
    /**
     * dirt tile
     */
    DIRT_TILE,

    /**
     * grass tile
     */
    GRASS_TILE,

    /**
     * water tile
     */
    WATER_TILE
}
