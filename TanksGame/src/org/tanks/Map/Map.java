package org.tanks.Map;

import org.tanks.Assets.Assets;
import org.tanks.Map.Tiles.*;

import java.awt.*;
import java.util.Random;

/**
 * The {@code Map} class is used for map data storage.
 *
 * @author Beldiman Vladislav
 */
public class Map {
    /**
     * dirt tile flyweight
     */
    private static final DirtTile DIRT_TILE = new DirtTile();

    /**
     * grass tile flyweight
     */
    private static final GrassTile GRASS_TILE = new GrassTile();

    /**
     * water tile flyweight
     */
    private static final WaterTile WATER_TILE = new WaterTile();

    /**
     * an array of tile references
     */
    private Tile[][] tiles;

    /**
     * the width of the map
     */
    private int WIDTH;

    /**
     * the height of map
     */
    private int HEIGHT;

    /**
     * Restarts the map, regenerating the tile distribution.
     *
     * @param WIDTH the new width of the map
     * @param HEIGHT the new height of the map
     */
    public void restart(int WIDTH, int HEIGHT) {
        this.WIDTH = WIDTH;
        this.HEIGHT = HEIGHT;
        tiles = new Tile[HEIGHT][WIDTH];

        generateMap();
    }

    /**
     * Load the map from saved data.
     *
     * @param tileData an array of tileTypes
     */
    public void load(TileType[][] tileData) {
        HEIGHT = tileData.length;
        WIDTH = tileData[0].length;
        tiles = new Tile[HEIGHT][WIDTH];

        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {

                tiles[y][x] = switch(tileData[y][x]) {
                    case GRASS_TILE -> Map.GRASS_TILE;
                    case DIRT_TILE -> Map.DIRT_TILE;
                    case WATER_TILE -> Map.WATER_TILE;
                };
            }
        }
    }

    /**
     * Draws map on graphics context.
     *
     * @param graphics a graphics context
     */
    public void draw(Graphics graphics) {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                tiles[y][x].draw(graphics, x, y);
            }
        }
    }

    /**
     * Obtains the tile at given coordinates.
     *
     * @param x x coordinate of the tile
     * @param y y coordinate of the tile
     * @return a tile referenceto one of the flyweights
     */
    public Tile getTile(int x, int y) {
        return tiles[y][x];
    }

    /**
     * Checks collision of impassible terrain with the rectangle. Map can have 1, 2 or 4 tiles
     * colliding with a rectangle, so the worst scenario only is checked.
     *
     * @param rectangle a rectangle representing the bounds of an entity
     * @return a flag that is true any collision with impassable terrain is found
     */
    public boolean checkCollision(Rectangle rectangle) {
        int x = (int) rectangle.getX();
        int y = (int) rectangle.getY();
        int width = (int) rectangle.getWidth();
        int height = (int) rectangle.getHeight();

        // Top left point
        int rectangleX1 = x / Assets.TILE_WIDTH;
        int rectangleY1 = y / Assets.TILE_HEIGHT;
        // Bottom right point
        int rectangleX2 = (x + width) / Assets.TILE_WIDTH;
        int rectangleY2 = (y + height) / Assets.TILE_HEIGHT;

        if (rectangleY2 >= tiles.length || rectangleX2 >= tiles[0].length) {
            return true;
        }

        return !(getTile(rectangleX1, rectangleY1).isPassable() &&
                 getTile(rectangleX2, rectangleY1).isPassable() &&
                 getTile(rectangleX1, rectangleY2).isPassable() &&
                 getTile(rectangleX2, rectangleY2).isPassable());
    }

    /**
     * Obtain tile data as tile types for saving.
     *
     * @return tile data for a save
     */
    public TileType[][] getTileData() {
        TileType[][] tileData = new TileType[HEIGHT][WIDTH];

        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                tileData[y][x] = getTile(x, y).getType();
            }
        }

        return tileData;
    }

    /**
     * Generates a map of randomly distributed flyweight tiles.
     */
    private void generateMap() {
        Random random = new Random();

        // Distribute tiles with
        // 20 % - dirtTile, 80 % - grassTile
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                if (random.nextInt(10) < 2) {
                    tiles[y][x] = DIRT_TILE;
                }
                else {
                    tiles[y][x] = GRASS_TILE;
                }
            }
        }

        // Lay River
        // On random column but the first and last (tank spawn points)
        int X_LEFT_OFFSET = 5;
        int X_RIGHT_OFFSET = 2;
        int x = X_LEFT_OFFSET + random.nextInt(WIDTH - X_LEFT_OFFSET - X_RIGHT_OFFSET);

        int MIN_LENGTH = 2;
        int MAX_LENGTH = 6;
        // Starting from a random point in the top part of the screen
        int from = random.nextInt(HEIGHT - MAX_LENGTH);
        // of length in [2, 7);
        int to = MIN_LENGTH + random.nextInt(MAX_LENGTH - MIN_LENGTH);

        for (int y = from; y < from + to; y++) {
            tiles[y][x] = WATER_TILE;
        }
    }
}
