package org.tanks.Input;

import java.awt.*;

/**
 * The {@code MouseInput} class is a container for a mouse input.
 *
 * @see MouseManager
 */
public class MouseInput {
    /**
     * a flag that is true, if the mouse is clicked
     */
    private final boolean mouseClicked;

    /**
     * a point representing the (x, y) coordinates of where the mouse was clicked
     */
    private final Point clickLocation;

    /**
     * Sole constructor
     *
     * @param mouseClicked a flag that determines whether the mouse was clicked during last key capture
     * @param clickLocation a point representing the x, y coordinates
     */
    MouseInput(boolean mouseClicked, Point clickLocation) {
        this.mouseClicked = mouseClicked;
        this.clickLocation = clickLocation;
    }

    /**
     * Obtains mouse clicked state.
     *
     * @return a flag that is true, if the mouse was clicked
     */
    public boolean isMouseClicked() { return mouseClicked; }

    /**
     * Obtains the location of the click.
     *
     * @return a point containing click location
     */
    public Point getClickLocation() { return clickLocation; }
}
