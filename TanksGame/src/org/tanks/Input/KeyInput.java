package org.tanks.Input;

import java.awt.event.KeyEvent;

/**
 * The {@code KeyInput} class is a container for {@code KeyManager} state.
 *
 * @see KeyManager
 * @author Beldiman Vladislav
 */
public class KeyInput {
    /**
     * up key code
     */
    public static final int UP = KeyEvent.VK_W;

    /**
     * down key code
     */
    public static final int DOWN = KeyEvent.VK_S;

    /**
     * left key code
     */
    public static final int LEFT = KeyEvent.VK_A;

    /**
     * right key code
     */
    public static final int RIGHT = KeyEvent.VK_D;

    /**
     * space key code
     */
    public static final int SPACE = KeyEvent.VK_SPACE;

    /**
     * escape key code
     */
    private static final int ESCAPE = KeyEvent.VK_ESCAPE;

    /**
     * key state
     */
    private final boolean[] KEY_STATE;

    /**
     * Sole constructor.
     *
     * @param KEY_STATE array of booleans that represents a key manager state
     */
    public KeyInput(boolean[] KEY_STATE) {
        this.KEY_STATE = KEY_STATE;
    }

    /**
     * Obtains pressed state of up key
     *
     * @return a flag that is true, if the up key state is true (it is pressed)
     */
    public boolean upPressed() { return KEY_STATE[UP]; }

    /**
     * Obtains pressed state of down key
     *
     * @return a flag that is true, if the down key state is true (it is pressed)
     */
    public boolean downPressed() { return KEY_STATE[DOWN]; }

    /**
     * Obtains pressed state of left key
     *
     * @return a flag that is true, if the left key state is true (it is pressed)
     */
    public boolean leftPressed() { return KEY_STATE[LEFT]; }

    /**
     * Obtains pressed state of right key
     *
     * @return a flag that is true, if the right key state is true (it is pressed)
     */
    public boolean rightPressed() { return KEY_STATE[RIGHT]; }

    /**
     * Obtains pressed state of space key
     *
     * @return a flag that is true, if the space key state is true (it is pressed)
     */
    public boolean spacePressed() { return KEY_STATE[SPACE]; }

    /**
     * Obtains pressed state of escape key
     *
     * @return a flag that is true, if the escape key state is true (it is pressed)
     */
    public boolean escapePressed() { return KEY_STATE[ESCAPE]; }

    /**
     * Obtains pressed state of demo key combination (D + E + M).
     *
     * @return a flag that is true, if the demo key combination state is true (all are pressed)
     */
    public boolean demoPressed() { return KEY_STATE[KeyEvent.VK_D] && KEY_STATE[KeyEvent.VK_E] &&
                                            KEY_STATE[KeyEvent.VK_M];}
}
