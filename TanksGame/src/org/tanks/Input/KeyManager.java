package org.tanks.Input;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * The {@code KeyManager} class is an extension of the class {@code KeyAdapter} class. It can return
 * a key input container that manages the captured key state.
 *
 * @see KeyInput
 * @author Beldiman Vladislav
 */
public class KeyManager extends KeyAdapter {
    /**
     * key state
     */
    private final boolean[] KEYS = new boolean[256];

    @Override
    public void keyPressed(KeyEvent keyEvent)
    {
        KEYS[keyEvent.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        KEYS[keyEvent.getKeyCode()] = false;
    }

    /**
     * Captures the current key state and returns it in a container.
     *
     * @return a key input container of the current key state
     */
    public KeyInput getInput() {
        return new KeyInput(KEYS);
    }
}
