package org.tanks.Input;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * The {@code MouseManager} class is an extension of {@code MouseAdapter} class. It can return a
 * mouse input container that manager the captured mouse state.
 *
 * @see MouseInput
 * @author Beldiman Vladislav
 */
public class MouseManager extends MouseAdapter {
    /**
     * a flag that is set when the mouse is clicked and reset when mouse input container is returned
     */
    boolean mouseClicked;

    /**
     * a point of the click coordinates
     */
    Point clickLocation;

    /**
     * Resets the mouse clicked flag on mouse input creation. This is done to prevent "infinite
     * clicking".
     *
     * @return a flag that holds the value of mouse clicked before the reset
     */
    public boolean consumeMouseClick() {
        boolean temp = mouseClicked;
        mouseClicked = false;
        return temp;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseClicked = true;
        clickLocation = e.getPoint();
    }

    /**
     * Captures the current mouse state and returns it in a container.
     *
     * @return a mouse input container of the current mouse state
     */
    public MouseInput getInput() { return new MouseInput(consumeMouseClick(), clickLocation); }
}
