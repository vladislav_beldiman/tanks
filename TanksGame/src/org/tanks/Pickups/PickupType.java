package org.tanks.Pickups;

import org.tanks.Effects.EffectType;

import java.awt.image.BufferedImage;

/**
 * {@code PickupType} instances are type objects for objects of class {@code Pickup}. They are used to store
 * the pickup texture and its dimensions.
 *
 * @see Pickup
 * @author Beldiman Vladislav
 */
public class PickupType {
    /**
     * a buffered image for the texture
     */
    private final BufferedImage TEXTURE;

    /**
     * width of pickup
     */
    private final int WIDTH;

    /**
     * height of pickup
     */
    private final int HEIGHT;

    /**
     * Sole Constructor.
     *
     * @param TEXTURE a buffered image for the texture
     * @param WIDTH width of pickup
     * @param HEIGHT height of pickup
     */
    public PickupType(BufferedImage TEXTURE, int WIDTH, int HEIGHT) {
        this.TEXTURE = TEXTURE;
        this.WIDTH = WIDTH;
        this.HEIGHT = HEIGHT;
    }

    /**
     * Obtains pickup texture.
     *
     * @return a buffered image representing the texture of the pickup
     */
    public BufferedImage getTexture() {
        return TEXTURE;
    }

    /**
     * Obtains pickup width.
     *
     * @return the width of the pickup
     */
    public int getWidth() {
        return WIDTH;
    }

    /**
     * Obtains pickup height.
     *
     * @return the height of the pickup
     */
    public int getHeight() {
        return HEIGHT;
    }
}
