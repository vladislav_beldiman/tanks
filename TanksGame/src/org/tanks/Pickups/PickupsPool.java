package org.tanks.Pickups;

import org.tanks.Assets.Assets;
import org.tanks.Effects.Effect;
import org.tanks.Entities.Entity;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Map.Map;

import java.awt.*;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * The {@code PickupsPool} class is used as an object pool for objects of type {@code Pickup}.
 *
 * @see Pickup
 * @see PickupType
 * @author Beldiman Vladislav
 */
public class PickupsPool {
    /**
     * minimum x spawn coordinate
     */
    private static final int MIN_SPAWN_X = 80;

    /**
     * maximum x spawn coordinate
     */
    private static final int MAX_SPAWN_X = 1200;

    /**
     * minimum y spawn coordinate
     */
    private static final int MIN_SPAWN_Y = 50;

    /**
     * maximum  spawn coordinate
     */
    private static final int MAX_SPAWN_Y = 700;

    /**
     * maximum number of tries to find a valid spawn location for pickups
     */
    private static final int MAX_SPAWN_TRIES = 100;

    /**
     * minimum spawn cooldown delay given in update cycles
     */
    private static final int MIN_DELAY = 5 * 60;

    /**
     * maximum spawn cooldown delay given in update cycles
     */
    private static final int MAX_SPREAD = 5 * 60;

    /**
     * current cooldown value given in update cycles
     */
    private int cooldown;

    /**
     * the maximum amount of active pickups
     */
    private static final int MAX_PICKUPS = 32;

    /**
     * random used for spawn location generation
     */
    private final Random random = new Random();

    /**
     * first inactive pickup in vector pool
     */
    private int firstInactive;

    /**
     * vector pool of pickups
     */
    private Vector<Pickup> pickups;


    /**
     * Restarts pickup pool. All pickups are initially inactive.
     */
    public void restart() {
        firstInactive = 0;

        pickups = new Vector<>(MAX_PICKUPS);

        for (int i = 0; i < MAX_PICKUPS; i++) {
            pickups.add(new Pickup());
        }

        cooldown = nextCooldown();
    }

    /**
     * Loads pickup pool from saved data.
     *
     * @param pickupsCoord a vector of points of the coordinates of pickups
     *
     */
    public void load(Vector<Point> pickupsCoord) {
        firstInactive = pickupsCoord.size();
        pickups = new Vector<>(MAX_PICKUPS);

        for (int i = 0; i < MAX_PICKUPS; i++) {
            pickups.add(new Pickup());
        }

        for (int i = 0; i < firstInactive; i++) {
            PickupType pickupType = new PickupType(Assets.UPGRADE, Assets.LEVEL_UP_WIDTH,
                                                                    Assets.LEVEL_UP_HEIGHT);
            Point point = pickupsCoord.get(i);

            pickups.get(i).spawn(pickupType, (int) point.getX(), (int) point.getY());
        }

        cooldown = nextCooldown();
    }

    /**
     * Updates all active pickups.
     *
     * @param map a map on which to spawn the next pickup
     * @param obstacleManager an obstacle manager to check collision with
     */
    public void update(Map map, ObstacleManager obstacleManager) {
        sortPool();

        cooldown--;

        if (cooldown <= 0) {
            PickupType pickupType = new PickupType(Assets.UPGRADE, Assets.LEVEL_UP_WIDTH,
                    Assets.LEVEL_UP_HEIGHT);
            spawn(map, obstacleManager, pickupType);
            cooldown = nextCooldown();
        }
    }

    /**
     * Draws all active enemies on graphics context.
     *
     * @param graphics graphics context
     */
    public void draw(Graphics graphics) {
        for (int i = 0; i < firstInactive; i++) {
            pickups.get(i).draw(graphics);
        }
    }

    /**
     * Checks collision of all active pickups with an entity.
     *
     * @param entity an entity to check collision with
     * @return a flag that is true if a collision was detected
     */
    public boolean checkEatingCollision(Entity entity) {
        boolean collided = false;

        for (int i = 0; i < firstInactive; i++) {
            collided |= pickups.get(i).checkEatingCollision(entity);
        }

        return collided;
    }

    /**
     * Obtains pickup positions
     *
     * @return a vector of points of projectile positions
     */
    public Vector<Point> getPickupsPosition() {
        Vector<Point> pickupsPosition = new Vector<>(firstInactive);

        for (int i = 0; i < firstInactive; i++) {
            Pickup pickup = pickups.get(i);
            pickupsPosition.add(new Point(pickup.getX(), pickup.getY()));
        }

        return pickupsPosition;
    }

    /**
     * Sorts the pool such that the first part of the vector pool contains only active enemies, and the other
     * only inactive ones.
     */
    private void sortPool() {
        for (int i = firstInactive - 1; i >= 0; i--) {
            Pickup pickup = pickups.get(i);

            if (pickup.eatten()) {
                firstInactive--;
                Collections.swap(pickups, i, firstInactive);
                pickup.refresh();
            }
        }
    }

    /**
     * Generates the next cooldown
     *
     * @return next generatore
     */
    private int nextCooldown() {
        return random.nextInt(MAX_SPREAD) + MIN_DELAY;
    }

    /**
     * Obtains x coordinate.
     *
     * @return x coordinate
     */
    private int getNextX() {
        return random.nextInt(MAX_SPAWN_X - MIN_SPAWN_X) + MIN_SPAWN_X;
    }

    /**
     * Obtains y coordinate.
     *
     * @return y coordinate
     */
    private int getNextY() {
        return random.nextInt(MAX_SPAWN_Y - MIN_SPAWN_Y) + MIN_SPAWN_Y;
    }

    /**
     * Respawns a pickup if vector pool has not reached its limit and a valid x spawn and.
     *
     * @param map a map to check collision with
     * @param obstacleManager an obstacle manager to chck allistions with
     * @param pickupType
     */
    private void spawn(Map map, ObstacleManager obstacleManager, PickupType pickupType) {
        if (firstInactive == MAX_PICKUPS) {
            return;
        }

        int x;
        int y;
        int width = pickupType.getWidth();
        int height = pickupType.getHeight();
        boolean badSpawn;

        int tries = MAX_SPAWN_TRIES;

        do {
            x = getNextX();
            y = getNextY();
            Rectangle bounds = new Rectangle(x, y, width, height);

            badSpawn = map.checkCollision(bounds)
                    || obstacleManager.checkCollision(bounds);

            tries--;
        } while (badSpawn && tries > 0);

        if (tries <= 0) {
            return;
        }

        Pickup nextSpawned = pickups.get(firstInactive);
        nextSpawned.spawn(pickupType, x, y);
        firstInactive++;
    }
}
