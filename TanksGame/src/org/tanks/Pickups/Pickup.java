package org.tanks.Pickups;

import org.tanks.Assets.Assets;
import org.tanks.Effects.EffectType;
import org.tanks.Entities.Entity;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The {@code Pickup} class implements general pickup behavior. It needs a pickup type type object so that
 * it can be display itself properly.
 *
 * @see PickupType
 * @author Beldiman Vladislav
 */
public class Pickup {
    /**
     * pickup type type object
     */
    private PickupType pickupType;

    /**
     * flag marking this for manager disposal
     */
    private boolean eatten;

    /**
     * x coordinate
     */
    private int x;

    /**
     * y coordinate
     */
    private int y;

    /**
     * Draws this on graphics context.
     *
     * @param graphics graphics context
     */
    public void draw(Graphics graphics) {
        BufferedImage texture = pickupType.getTexture();

        graphics.drawImage(texture, x, y,null);
        // draw hitbox. used for debugging purpose only
//        graphics.setColor(Color.red);
//        graphics.drawRect(x, y, Assets.LEVEL_UP_WIDTH, Assets.LEVEL_UP_HEIGHT);
    }

    /**
     * Obtains x coordinate.
     *
     * @return x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Obtains y coordinate.
     *
     * @return y coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Obtains information about disposal of this.
     *
     * @return a flag that is true if this should be disposed
     */
    public boolean eatten() {
        return eatten;
    }

    /**
     * Sets disposal flag.
     */
    public void eat() {
        eatten = true;
    }

    /**
     * Resets disposal flag.
     */
    public void refresh() {
        eatten = false;
    }

    /**
     * Respawns this pickup with another pickup type and coordinates.
     *
     * @param pickupType new pickup type type object
     * @param x new x coordinate
     * @param y new y coordinate
     */
    public void spawn(PickupType pickupType, int x, int y) {
        this.pickupType = pickupType;
        this.x = x;
        this.y = y;
    }

    /**
     * Checks collision with an entity and sets the disposal flag if collision is detected.
     *
     * @param entity an entity to check collision with
     * @return a flag that is true collision was detected
     */
    public boolean checkEatingCollision(Entity entity) {
        Rectangle bounds = new Rectangle(x, y, pickupType.getWidth(), pickupType.getHeight());
        boolean collided = bounds.intersects(entity.getBounds());
        if (collided) {
            eat();
        }

        return collided;
    }
}
