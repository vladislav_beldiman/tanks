package org.tanks.Assets;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * The {@code Assets} class loads and provides access to all assets and their actual sizes.
 *
 * @author Beldiman Vladislav
 */
public class Assets {
    /**
     * a buffered image that is completely transparent
     */
    public static BufferedImage BLANK;

    /**
     * a buffered image of the grass asset
     */
    public static BufferedImage GRASS;

    /**
     * a buffered image of the dirt asset
     */
    public static BufferedImage DIRT;

    /**
     * a buffered image of the water asset
     */
    public static BufferedImage WATER;

    /**
     * a buffered image of the hedgehog asset
     */
    public static BufferedImage HEDGEHOG;

    /**
     * a buffered image of the tree asset
     */
    public static BufferedImage TREE;

    /**
     * a buffered image of the logs asset
     */
    public static BufferedImage LOGS;

    /**
     * a buffered image of the upgrade asset
     */
    public static BufferedImage UPGRADE;

    /**
     * the number of possible directions
     * <p>
     * UP - DOWN - LEFT - RIGHT
     * 0  - 1    - 2    - 3
     */
    private static final int DIRECTIONS = 4;

    /**
     * the number of available tank levels
     */
    private static final int LEVELS = 3;

    /**
     * the number of animation frames
     */
    private static final int FRAMES = 4;

    /**
     * an array of buffered images of the player tank asset for all levels for all directions
     */
    public static final BufferedImage[][] PLAYER = new BufferedImage[LEVELS][DIRECTIONS];

    /**
     * an array of buffered images of the enemy tank asset for all levels for all directions
     */
    public static final BufferedImage[][] ENEMY = new BufferedImage[LEVELS][DIRECTIONS];

    /**
     * an array of buffered images of the shell asset for all directions
     */
    public static final BufferedImage[] SHELL = new BufferedImage[DIRECTIONS];

    /**
     * an array of buffered images of the rocket asset for all directions
     */
    public static final BufferedImage[] ROCKET = new BufferedImage[DIRECTIONS];

    /**
     * an array of buffered images of the flash animation asset for all frames
     */
    public static final BufferedImage[] FLASH = new BufferedImage[FRAMES];

    /**
     * an array of buffered images of the level up animation asset for all frames
     */
    public static final BufferedImage[] LEVEL_UP = new BufferedImage[FRAMES];

    /**
     * the actual width of the tile asset
     */
    public static final int TILE_WIDTH = 64;

    /**
     * the actual height of the tile asset
     */
    public static final int TILE_HEIGHT = 64;

    /**
     * the actual width of the hedgehog asset
     */
    public static final int HEDGEHOG_WIDTH = 24;

    /**
     * the actual height of the hedgehog asset
     */
    public static final int HEDGEHOG_HEIGHT = 26;

    /**
     * the actual width of the tree asset
     */
    public static final int TREE_WIDTH = 36;

    /**
     * the actual height of the tree asset
     */
    public static final int TREE_HEIGHT = 30;

    /**
     * the actual width of the logs asset
     */
    public static final int LOGS_WIDTH = 33;

    /**
     * the actual height of the logs asset
     */
    public static final int LOGS_HEIGHT = 38;

    /**
     * the actual width of the level up asset
     */
    public static final int LEVEL_UP_WIDTH = 22;

    /**
     * the actual height of the level up asset
     */
    public static final int LEVEL_UP_HEIGHT = 30;

    /**
     * the actual width of the tank asset for all levels
     */
    public static final int TANK_WIDTH = 44;

    /**
     * the actual height of the tank asset for all levels
     */
    public static final int TANK_HEIGHT = 44;

    /**
     * the actual width of the shell asset
     */
    public static final int SHELL_WIDTH = 11;

    /**
     * the actual height of the shell asset
     */
    public static final int SHELL_HEIGHT = 11;

    /**
     * the actual width of the rocket asset
     */
    public static final int ROCKET_WIDTH = 14;

    /**
     * the actual height of the rocket asset
     */
    public static final int ROCKET_HEIGHT = 18;

    /**
     * Loads sprite sheet image and initializes all asset images by cropping fixed parts from it.
     *
     * @throws IOException if an I/O exception occurred during sprite sheet image load
     * @see SpriteSheet
     */
    public static void initAssets() throws IOException {
        String spriteSheetPath = "/textures/SpriteSheet.png";
        SpriteSheet sheet = new SpriteSheet(SpriteSheet.loadImage(spriteSheetPath));

        BLANK = sheet.crop(7, 0);

        GRASS = sheet.crop(0,0);
        DIRT = sheet.crop(1, 0);
        WATER = sheet.crop(2, 0);
        HEDGEHOG = sheet.crop(3, 0);
        TREE = sheet.crop(4, 0);
        LOGS = sheet.crop(5, 0);
        UPGRADE = sheet.crop(6, 0);

        // PLAYER LEVEL 1
        PLAYER[0][0] = sheet.crop(0, 1);
        PLAYER[0][1] = sheet.crop(0, 2);
        PLAYER[0][2] = sheet.crop(0, 3);
        PLAYER[0][3] = sheet.crop(0, 4);

        // PLAYER LEVEL 2
        PLAYER[1][0] = sheet.crop(1, 1);
        PLAYER[1][1] = sheet.crop(1, 2);
        PLAYER[1][2] = sheet.crop(1, 3);
        PLAYER[1][3] = sheet.crop(1, 4);

        // PLAYER LEVEL 3
        PLAYER[2][0] = sheet.crop(2, 1);
        PLAYER[2][1] = sheet.crop(2, 2);
        PLAYER[2][2] = sheet.crop(2, 3);
        PLAYER[2][3] = sheet.crop(2, 4);

        // ENEMY LEVEL 1
        ENEMY[0][0] = sheet.crop(3, 1);
        ENEMY[0][1] = sheet.crop(3, 2);
        ENEMY[0][2] = sheet.crop(3, 3);
        ENEMY[0][3] = sheet.crop(3, 4);

        // ENEMY LEVEL 2
        ENEMY[1][0] = sheet.crop(4, 1);
        ENEMY[1][1] = sheet.crop(4, 2);
        ENEMY[1][2] = sheet.crop(4, 3);
        ENEMY[1][3] = sheet.crop(4, 4);

        // ENEMY LEVEL 3
        ENEMY[2][0] = sheet.crop(5, 1);
        ENEMY[2][1] = sheet.crop(5, 2);
        ENEMY[2][2] = sheet.crop(5, 3);
        ENEMY[2][3] = sheet.crop(5, 4);

        // SHELL
        SHELL[0] = sheet.crop(6, 1);
        SHELL[1] = sheet.crop(6, 2);
        SHELL[2] = sheet.crop(6, 3);
        SHELL[3] = sheet.crop(6, 4);

        // ROCKET
        ROCKET[0] = sheet.crop(7, 1);
        ROCKET[1] = sheet.crop(7, 2);
        ROCKET[2] = sheet.crop(7, 3);
        ROCKET[3] = sheet.crop(7, 4);

        // FLASH
        FLASH[0] = sheet.crop(8, 1);
        FLASH[1] = sheet.crop(8, 2);
        FLASH[2] = sheet.crop(8, 3);
        FLASH[3] = sheet.crop(8, 4);

        // LEVEL_UP
        LEVEL_UP[0] = sheet.crop(9, 1);
        LEVEL_UP[1] = sheet.crop(9, 2);
        LEVEL_UP[2] = sheet.crop(9, 3);
        LEVEL_UP[3] = sheet.crop(9, 4);
    }
}
