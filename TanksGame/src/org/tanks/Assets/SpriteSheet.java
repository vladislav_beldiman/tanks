package org.tanks.Assets;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * The {@code SpriteSheet} class is used for loading the sprite sheet and cropping textures for assets.
 *
 * @author Beldiman Vladislav
 */
public class SpriteSheet {
    /**
     * The loaded sprite sheet
     */
    private final BufferedImage spriteSheet;

    /**
     * The fixed width of assets in the image
     */
    public static final int ASSET_WIDTH = 64;

    /**
     * The fixed height of assets in the image
     */
    public static final int ASSET_HEIGHT = 64;

    /**
     * Sole constructor. Saves a reference to the loaded image.
     *
     * @param spriteSheet the loaded buffered image
     */
    public SpriteSheet(BufferedImage spriteSheet) {
        this.spriteSheet = spriteSheet;
    }

    /**
     * Used for loading sprite sheets.
     *
     * @param path the path to the image. The root is in the res directory.
     * @return the loaded buffered image
     * @throws IOException if an I/O exception occurs during image read
     */
    public static BufferedImage loadImage(String path) throws IOException {
        return ImageIO.read(SpriteSheet.class.getResource(path));
    }

    /**
     *
     *
     * @param x the x coordinate of the asset in the sprite sheet divided by the {@code ASSET_WIDTH}
     * @param y the y coordinate of the asset in the sprite sheet divided by the {@code ASSET_HEIGHT}
     * @return a sub buffered image of the asset
     */
    public BufferedImage crop(int x, int y) {
        return spriteSheet.getSubimage(x * ASSET_WIDTH, y * ASSET_HEIGHT,
                ASSET_WIDTH, ASSET_HEIGHT);
    }
}
