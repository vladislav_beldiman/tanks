package org.tanks;

import org.tanks.AudioService.FullAudioService;
import org.tanks.AudioService.AudioService;
import org.tanks.AudioService.NullAudioService;
import org.tanks.DatabaseService.DatabaseService;
import org.tanks.DatabaseService.FullDatabaseService;
import org.tanks.DatabaseService.NullDatabaseService;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * The {@code Locator} is a Service Locator for 2 services: {@code AudioService} and {@code DatabaseService}.
 * It provides seamless access to these services, even if their constructors, or any of the methods  fail.
 *
 * @see AudioService
 * @see DatabaseService
 * @author Beldiman Vladislav
 */
public class Locator {
    /**
     * audio service with full capabilities
     */
    private FullAudioService fullAudioService;

    /**
     * audio service reference
     */
    private AudioService audioService;

    /**
     * database service
     */
    private DatabaseService databaseService;

    /**
     * Sole Constructor. Tries to initialize all services at full capability. If any fail, they are set to
     * the null service version instead.
     */
    public Locator() {
        try {
            databaseService = new FullDatabaseService();
        }
        catch (SQLException | ClassNotFoundException sqlException) {
            sqlException.printStackTrace();
            databaseService = new NullDatabaseService();
        }

        try {
            fullAudioService = new FullAudioService();
            audioService = fullAudioService;
        }
        catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
            audioService = new NullAudioService();
        }
    }

    /**
     * Obtains the current audio service
     *
     * @return current audio service
     */
    public AudioService getAudioService() {
        return audioService;
    }

    /**
     * Mutes audios by stopping the music if playing and setting audio service to null audio service.
     */
    public void muteAudio() {
        if (getAudioOn()) {
            audioService.stopMusic();
        }
        audioService = new NullAudioService();
    }

    /**
     * Turns on audio by setting audio service to full audio service.
     */
    public void turnOnAudio() {
        audioService = fullAudioService;
        audioService.playMusic();
    }

    /**
     * Turns audio on or off depending on its current state.
     */
    public void switchAudio() {
        if (getAudioOn()) {
            muteAudio();
        }
        else {
            turnOnAudio();
        }
    }

    /**
     * Obtains audio not muted state.
     *
     * @return true if audio not muted
     */
    public boolean getAudioOn() {
        return audioService == fullAudioService;
    }

    /**
     * Obtains the current database service.
     *
     * @return current database service
     */
    public DatabaseService getDatabaseService() {
        return databaseService;
    }
}
