package org.tanks.DatabaseService;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Objects.Obstacles.ObstacleType;
import org.tanks.Entities.Objects.Projectiles.ProjectileType;
import org.tanks.Entities.Allegiance;
import org.tanks.Map.Tiles.TileType;

import java.awt.*;
import java.util.Arrays;
import java.util.Vector;

/**
 * A container for save data.
 *
 * @author Beldiman Vladislav
 */
public class SaveData {
    /**
     * the difficulty in the save
     */
    private final int difficulty;

    /**
     * the score in the save
     */
    private final int score;

    /**
     * the point that represents the player coordinates
     */
    private final Point playerCoord;

    /**
     * the player level
     */
    private final int playerLevel;

    /**
     * the player health points
     */
    private final int playerHp;

    /**
     * the player lives
     */
    private final int playerLives;


    /**
     * the vector of points that represents the enemy coordinates
     */
    private final Vector<Point> enemyCoords;

    /**
     * the vector that represents the enemy levels
     */
    private final Vector<Integer> enemyLevels;

    /**
     * the vector that represents the enemy health points
     */
    private final Vector<Integer> enemyHps;

    /**
     * the vector of points that represents the obstacle coordinates
     */
    private final Vector<Point> obstacleCoords;

    /**
     * the vector of obstacle types that represents the types of the obstacles
     */
    private final Vector<ObstacleType> obstacleTypes;

    /**
     * the vector of points that represents the pickups coordinates
     */
    private final Vector<Point> pickupCoords;

    /**
     * the vector of points that represents the projectile coordinates
     */
    private final Vector<Point> projectileCoords;

    /**
     * the vector of directions that represents the flying direction of the projectiles
     */
    private final Vector<Direction> projectileDirections;

    /**
     * the vector of projectile types that represents the types of the projectiles
     */
    private final Vector<ProjectileType> projectileTypes;

    /**
     * the vector of allegiance types that represents the allegiances of the projectiles
     */
    private final Vector<Allegiance> projectileAllegiances;

    /**
     * an array of the tile types of the map
     */
    private final TileType[][] tiles;

    /**
     * Default constructor. Creates a container of default values for a game. Used when no save
     * data is available or an error has occurred during save load.
     */
    public SaveData() {
        difficulty = 0;
        score = 0;
        playerCoord = new Point(PlayerManager.PLAYER_SPAWN_X, PlayerManager.PLAYER_SPAWN_Y);
        playerLevel = 0;
        playerHp = 1;
        playerLives =  3;
        enemyCoords = new Vector<>();
        enemyLevels = new Vector<>();
        enemyHps = new Vector<>();
        obstacleCoords = new Vector<>();
        obstacleTypes = new Vector<>();
        pickupCoords = new Vector<>();
        projectileCoords = new Vector<>();
        projectileDirections = new Vector<>();
        projectileTypes = new Vector<>();
        projectileAllegiances = new Vector<>();
        tiles = new TileType[12][20];

        for (TileType[] tile : tiles) {
            Arrays.fill(tile, TileType.DIRT_TILE);
        }
    }

    /**
     * Constructor called on successful save creation / loading.
     *
     * @param difficulty the current difficulty
     * @param score the current score
     * @param playerCoord the point of the current player coordinates
     * @param playerLevel the current player level
     * @param playerHp the current player heath points
     * @param playerLives the current player lives
     * @param enemyCoords the vector of points of the current enemy coordinates
     * @param enemyLevels the vector of the current enemy levels
     * @param enemyHps the vector of the current enemy health points
     * @param obstacleCoords the vector of points of the current obstacle coordinates
     * @param obstacleTypes the vector of obstacle types
     * @param pickupCoords the vector of points of the current pickup coordinates
     * @param projectileCoords the vector of points of the current projectile coordinates
     * @param projectileDirections the vector of directions of projectiles
     * @param projectileTypes the vector of projectile types
     * @param projectileAllegiances the vector of projectile allegiances
     * @param tiles the array of the tile types of the map
     * @see ObstacleType
     * @see Direction
     * @see ProjectileType
     * @see Allegiance
     * @see TileType
     */
    public SaveData(int difficulty, int score, Point playerCoord, int playerLevel, int playerHp,
                    int playerLives, Vector<Point> enemyCoords, Vector<Integer> enemyLevels,
                    Vector<Integer> enemyHps, Vector<Point> obstacleCoords,
                    Vector<ObstacleType> obstacleTypes, Vector<Point> pickupCoords,
                    Vector<Point> projectileCoords, Vector<Direction> projectileDirections,
                    Vector<ProjectileType> projectileTypes, Vector<Allegiance> projectileAllegiances,
                    TileType[][] tiles) {

        this.difficulty = difficulty;
        this.score = score;
        this.playerCoord = playerCoord;
        this.playerLevel = playerLevel;
        this.playerHp = playerHp;
        this.playerLives = playerLives;
        this.enemyCoords = enemyCoords;
        this.enemyLevels = enemyLevels;
        this.enemyHps = enemyHps;
        this.obstacleCoords = obstacleCoords;
        this.obstacleTypes = obstacleTypes;
        this.pickupCoords = pickupCoords;
        this.projectileCoords = projectileCoords;
        this.projectileDirections = projectileDirections;
        this.projectileTypes = projectileTypes;
        this.projectileAllegiances = projectileAllegiances;
        this.tiles = tiles;
    }

    /**
     * Obtain the difficulty in the save
     * 
     * @return the difficulty in the save
     */
    public int getDifficulty() { return difficulty; }

    /**
     * Obtain the score in the save
     *
     * @return the score in the save
     */
    public int getScore() {
        return score;
    }

    /**
     * Obtain the player coordinates in the save
     *
     * @return the point of the player coordinates in the save
     */
    public Point getPlayerCoord() {
        return playerCoord;
    }

    /**
     * Obtain the player level in the save
     *
     * @return the player level in the save
     */
    public int getPlayerLevel() { return playerLevel; }

    /**
     * Obtain the player health points in the save
     *
     * @return the player health points in the save
     */
    public int getPlayerHp() {
        return playerHp;
    }

    /**
     * Obtain the player lives in the save
     *
     * @return the player lives in the save
     */
    public int getPlayerLives() {
        return playerLives;
    }

    /**
     * Obtain the enemy coordinates in the save
     *
     * @return the vector of points of the enemy coordinates in the save
     */
    public Vector<Point> getEnemyCoords() {
        return enemyCoords;
    }

    /**
     * Obtain the enemy levels in the save
     *
     * @return the vector of the enemy level in the save
     */
    public Vector<Integer> getEnemyLevels() {
        return enemyLevels;
    }

    /**
     * Obtain the enemy health points in the save
     *
     * @return the vector of enemy health points in the save
     */
    public Vector<Integer> getEnemyHps() {
        return enemyHps;
    }

    /**
     * Obtain the obstacle coordinates in the save
     *
     * @return the vector of points of the obstacle coordinates in the save
     */
    public Vector<Point> getObstacleCoords() {
        return obstacleCoords;
    }

    /**
     * Obtain the obstacle types in the save
     *
     * @return the vector of obstacle types in the save
     * @see ObstacleType
     */
    public Vector<ObstacleType> getObstacleTypes() {
        return obstacleTypes;
    }

    /**
     * Obtain the pickup coordinates in the save
     *
     * @return the vector of points of the pickup coordinates in the save
     */
    public Vector<Point> getPickupCoords() {
        return pickupCoords;
    }

    /**
     * Obtain the projectile coordinates in the save
     *
     * @return the vector of points of the projectile coordinates in the save
     */
    public Vector<Point> getProjectileCoords() {
        return projectileCoords;
    }

    /**
     * Obtain the projectile directions in the save
     *
     * @return the vector of directions of the projectiles in the save
     * @see Direction
     */
    public Vector<Direction> getProjectileDirections() {
        return projectileDirections;
    }

    /**
     * Obtain the projectile types in the save
     *
     * @return the vector of projectile types in the save
     * @see ProjectileType
     */
    public Vector<ProjectileType> getProjectileTypes() {
        return projectileTypes;
    }

    /**
     * Obtain the projectile allegiances in the save
     *
     * @return the vector of projectile allegiances in the save
     * @see Allegiance
     */
    public Vector<Allegiance> getProjectileAllegiances() {
        return projectileAllegiances;
    }

    /**
     * Obtain the tile types of the map in the save
     *
     * @return the array of tile types of the map in the save
     * @see TileType
     */
    public TileType[][] getTiles() {
        return tiles;
    }
}
