package org.tanks.DatabaseService;

/**
 * The {@code DatabaseService} interface represents a service which provides access to a database
 * used for saving game data.
 *
 * @author Beldiman Vladislav
 */
public interface DatabaseService {
    /**
     * Obtains a setting from the database.
     *
     * @param setting a string naming the setting needed
     * @return the value of the setting in the database
     */
    int getSetting(String setting);

    /**
     * Updates a setting in the database.
     *
     * @param setting a string naming the setting to be updated
     * @param newValue the new value for the setting
     */
    void updateSetting(String setting, int newValue);

    /**
     * Obtains last saved data.
     *
     * @return a save data container of the last saved data from the data base
     */
    SaveData getSaveData();

    /**
     * Updates last saved data in the database.
     *
     * @param saveData a save data container to be saved
     */
    void updateSaveData(SaveData saveData);
}
