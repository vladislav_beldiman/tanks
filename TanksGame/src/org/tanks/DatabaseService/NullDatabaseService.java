package org.tanks.DatabaseService;

/**
 * The {@code NullDatabaseService} class provides a "do nothing" (and return default) implementation
 * for all database service methods.
 *
 * @see DatabaseService
 * @author Beldiman Vladislav
 */
public class NullDatabaseService implements DatabaseService {
    public int getSetting(String setting) {
        /* do nothing */
        return 0;
    }

    public void updateSetting(String setting, int newValue) {
        /* do nothing */
    }

    public SaveData getSaveData() {
        return new SaveData();
    }

    public void updateSaveData(SaveData saveData) {
        /* do nothing */
    }
}
