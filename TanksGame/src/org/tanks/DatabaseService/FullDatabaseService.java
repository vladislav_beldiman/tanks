package org.tanks.DatabaseService;

import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Objects.Obstacles.ObstacleType;
import org.tanks.Entities.Objects.Projectiles.ProjectileType;
import org.tanks.Entities.Allegiance;
import org.tanks.Map.Tiles.TileType;

import java.awt.*;
import java.sql.*;
import java.util.Vector;

/**
 * The {@code FullDatabaseService} class provides full saving game data to database capabilities.
 *
 * @see DatabaseService
 * @author Beldiman Vladislav
 */
public class FullDatabaseService implements DatabaseService {
    /**
     * Sole contructor. Tries to connect to database, create settings table and entry.
     * Throws on failure to do so.
     *
     * @throws ClassNotFoundException if the sqlite java data base connection class cannot be located
     * @throws SQLException if a database access errors occur
     */
    public FullDatabaseService() throws ClassNotFoundException, SQLException {
        try (Connection connection = connect();
             Statement statement = connection.createStatement()) {

            // sql for creating settings table, if it does not already exist
            String createSettings = "CREATE TABLE IF NOT EXISTS settings (" +
                    "id INTEGER UNIQUE NOT NULL, " +
                    "difficulty INTEGER NOT NULL, " +
                    "soundMuted INTEGER NOT NULL, " +
                    "highscore INTEGER NOT NULL);";
            statement.executeUpdate(createSettings);

            // sql for inserting settings entry into settings table, if it does not already exist
            String createSettingsEntry = "INSERT OR IGNORE INTO settings" +
                    "(id, difficulty, soundMuted, highscore)" +
                    "VALUES (0, 0, 0, 0);";
            statement.executeUpdate(createSettingsEntry);
        }
    }

    @Override
    public int getSetting(String setting) {
        // sql for getting a setting from the settings table
        String getSetting = "SELECT " + setting + " FROM settings WHERE id = 0;";

        try (Connection connection = connect();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(getSetting)) {

            return resultSet.getInt(setting);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void updateSetting(String setting, int newValue) {
        // sql for updating a setting to a new value in the settings table
        String updateSettings = "UPDATE settings SET " + setting + " = " + newValue + " WHERE id = 0;";

        try (Connection connection = connect();
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(updateSettings);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SaveData getSaveData() {
        try (Connection connection = connect()) {
            // prepare default player data
            Point playerCoord = new Point(PlayerManager.PLAYER_SPAWN_X, PlayerManager.PLAYER_SPAWN_Y);
            int playerLevel = 0;
            int playerHp = 1;

            // prepare default enemy data
            Vector<Point> enemyCoords = new Vector<>();
            Vector<Integer> enemyLevels = new Vector<>();
            Vector<Integer> enemyHps = new Vector<>();

            // prepare default obstacle data
            Vector<Point> obstacleCoords = new Vector<>();
            Vector<ObstacleType> obstacleTypes = new Vector<>();

            // prepare default pickup data
            Vector<Point> pickupCoords = new Vector<>();

            // prepare default projectile data
            Vector<Point> projectileCoords = new Vector<>();
            Vector<Direction> projectileDirections = new Vector<>();
            Vector<ProjectileType> projectileTypes = new Vector<>();
            Vector<Allegiance> projectileAllegiances = new Vector<>();

            // prepare default map tile data
            TileType[][] tiles = new TileType[12][20];

            // sql for selecting all entries of an id group ("player", "enemy", ...) from saved_points
            String getSavedPointsData = "SELECT * FROM saved_points WHERE id = ?;";

            try (PreparedStatement preparedStatement = connection.prepareStatement(getSavedPointsData)) {

                // get player data
                preparedStatement.setString(1, "player");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        playerCoord = new Point(resultSet.getInt("x"),
                                                resultSet.getInt("y"));
                        playerLevel = resultSet.getInt("level_or_direction");
                        playerHp = resultSet.getInt("hp_or_type");
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }

                // get enemy data
                preparedStatement.setString(1, "enemy");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        enemyCoords.add(new Point(resultSet.getInt("x"),
                                                    resultSet.getInt("y")));
                        enemyLevels.add(resultSet.getInt("level_or_direction"));
                        enemyHps.add(resultSet.getInt("hp_or_type"));
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }

                // get obstacle data
                preparedStatement.setString(1, "obstacle");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        obstacleCoords.add(new Point(resultSet.getInt("x"),
                                                     resultSet.getInt("y")));
                        obstacleTypes.add(ObstacleType.values()[resultSet.getInt("hp_or_type")]);
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }

                // get pickup data
                preparedStatement.setString(1, "pickup");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        pickupCoords.add(new Point(resultSet.getInt("x"),
                                                    resultSet.getInt("y")));
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }

                // get projectile data
                preparedStatement.setString(1, "projectile");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        projectileCoords.add(new Point(resultSet.getInt("x"),
                                                        resultSet.getInt("y")));
                        projectileDirections.add(
                                Direction.values()[resultSet.getInt("level_or_direction")]
                        );
                        projectileTypes.add(
                                ProjectileType.values()[resultSet.getInt("hp_or_type")]
                        );
                        projectileAllegiances.add(
                                Allegiance.values()[resultSet.getInt("allegiance")]
                        );
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }

                // get map tile data
                preparedStatement.setString(1, "tile");

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        int x = resultSet.getInt("x");
                        int y = resultSet.getInt("y");
                        tiles[y][x] = TileType.values()[resultSet.getInt("hp_or_type")];
                    }
                }
                catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                    return new SaveData();
                }
            }
            catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return new SaveData();
            }

            // prepare save settings defaults
            int difficulty = 0;
            int score = 0;
            int playerLives = 3;

            // sql for selecting all save settings from save_settings
            String getSaveSettingsData = "SELECT * FROM save_settings";

            // get save settings
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(getSaveSettingsData)) {

                if (resultSet.next()) {
                    difficulty = resultSet.getInt("difficulty");
                    score = resultSet.getInt("score");
                    playerLives = resultSet.getInt("lives");
                }
            }
            catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return new SaveData();
            }

            return new SaveData(difficulty, score, playerCoord, playerLevel, playerHp, playerLives, enemyCoords,
                    enemyLevels, enemyHps, obstacleCoords,obstacleTypes, pickupCoords, projectileCoords,
                    projectileDirections, projectileTypes, projectileAllegiances, tiles);
        }
        catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return new SaveData();
        }
    }

    @Override
    public void updateSaveData(SaveData saveData) {
        // sql for inserting entries into saved_points
        String insertPointString = "INSERT INTO saved_points " +
                "(id, x, y, level_or_direction, hp_or_type, allegiance)" +
                "VALUES (?, ?, ?, ?, ?, ?);";

        // sql for inserting save settings into save_settings
        String insertSettingsString = "INSERT INTO save_settings " +
                "(difficulty, score, lives) VALUES (?, ?, ?);";

        try (Connection connection = connect();
             Statement statement = connection.createStatement()) {

            connection.setAutoCommit(false);

            // sql dropping previous save data from saved_points, if there is any
            String dropSavedPoints = "DROP TABLE IF EXISTS saved_points;";
            statement.executeUpdate(dropSavedPoints);

            // sql for creating a new saved_points table
            String createSavedPoints = "CREATE TABLE saved_points (" +
                    "id TEXT NOT NULL, " +
                    "x INTEGER NOT NULL, " +
                    "y INTEGER NOT NULL, " +
                    "level_or_direction INTEGER, " +
                    "hp_or_type INTEGER, " +
                    "allegiance INTEGER);";
            statement.executeUpdate(createSavedPoints);

            try (PreparedStatement insertPoint = connection.prepareStatement(insertPointString)) {

                // save player data
                Point playerCoord = saveData.getPlayerCoord();
                int playerLevel = saveData.getPlayerLevel();
                int playerHp = saveData.getPlayerHp();
                insertPoint.setString(1, "player");
                insertPoint.setInt(2, (int) playerCoord.getX());
                insertPoint.setInt(3, (int) playerCoord.getY());
                insertPoint.setInt(4, playerLevel);
                insertPoint.setInt(5, playerHp);
                insertPoint.setNull(6, Types.INTEGER);
                insertPoint.executeUpdate();

                // save enemy data
                Vector<Point> points = saveData.getEnemyCoords();
                Vector<Integer> enemiesLevel = saveData.getEnemyLevels();
                Vector<Integer> enemiesHp = saveData.getEnemyHps();
                insertPoint.setString(1, "enemy");
                insertPoint.setNull(6, Types.INTEGER);

                for (int i = 0; i < points.size(); i++) {
                    Point enemyCoord = points.get(i);
                    insertPoint.setInt(2, (int) enemyCoord.getX());
                    insertPoint.setInt(3, (int) enemyCoord.getY());
                    insertPoint.setInt(4, enemiesLevel.get(i));
                    insertPoint.setInt(5, enemiesHp.get(i));
                    insertPoint.executeUpdate();
                }

                // save obstacle data
                points = saveData.getObstacleCoords();
                Vector<ObstacleType> obstaclesType = saveData.getObstacleTypes();
                insertPoint.setString(1, "obstacle");
                insertPoint.setNull(4, Types.INTEGER);
                insertPoint.setNull(6, Types.INTEGER);

                for (int i = 0; i < points.size(); i++) {
                    Point obstacleCoord = points.get(i);
                    insertPoint.setInt(2, (int) obstacleCoord.getX());
                    insertPoint.setInt(3, (int) obstacleCoord.getY());
                    insertPoint.setNull(5, obstaclesType.get(i).ordinal());
                    insertPoint.executeUpdate();
                }

                // save pickup data
                points = saveData.getPickupCoords();
                insertPoint.setString(1, "pickup");
                insertPoint.setNull(4, Types.INTEGER);
                insertPoint.setNull(5, Types.INTEGER);
                insertPoint.setNull(6, Types.INTEGER);

                for (Point pickupCoord : points) {
                    insertPoint.setInt(2, (int) pickupCoord.getX());
                    insertPoint.setInt(3, (int) pickupCoord.getY());
                    insertPoint.executeUpdate();
                }

                // save projectile data
                points = saveData.getProjectileCoords();
                Vector<Direction> projectilesDirection = saveData.getProjectileDirections();
                Vector<ProjectileType> projectilesType = saveData.getProjectileTypes();
                Vector<Allegiance> projectilesAllegiance = saveData.getProjectileAllegiances();
                insertPoint.setString(1, "projectile");

                for (int i = 0; i < points.size(); i++) {
                    Point projectileCoord = points.get(i);
                    insertPoint.setInt(2, (int) projectileCoord.getX());
                    insertPoint.setInt(3, (int) projectileCoord.getY());
                    insertPoint.setInt(4, projectilesDirection.get(i).ordinal());
                    insertPoint.setInt(5, projectilesType.get(i).ordinal());
                    insertPoint.setInt(6, projectilesAllegiance.get(i).ordinal());
                    insertPoint.executeUpdate();
                }

                // save map tile data
                TileType[][] tilesCoord = saveData.getTiles();
                insertPoint.setString(1, "tile");
                insertPoint.setNull(4, Types.INTEGER);
                insertPoint.setNull(6, Types.INTEGER);

                for (int y = 0; y < tilesCoord.length; y++) {
                    for (int x = 0; x < tilesCoord[y].length; x++) {
                        insertPoint.setInt(2, x);
                        insertPoint.setInt(3, y);
                        insertPoint.setInt(5, tilesCoord[y][x].ordinal());
                        insertPoint.executeUpdate();
                    }
                }
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return;
            }

            // sql dropping previous save data from save_settings, if there is any
            String dropSaveSettings = "DROP TABLE IF EXISTS save_settings;";
            statement.executeUpdate(dropSaveSettings);

            // sql for creating a new save_settings table
            String createSaveSettings = "CREATE TABLE save_settings (" +
                    "difficulty INTEGER NOT NULL, " +
                    "score INTEGER NOT NULL, " +
                    "lives INTEGER);";
            statement.executeUpdate(createSaveSettings);

            try (PreparedStatement insertSettings = connection.prepareStatement(insertSettingsString)) {

                // save save settings
                insertSettings.setInt(1, saveData.getDifficulty());
                insertSettings.setInt(2, saveData.getScore());
                insertSettings.setInt(3, saveData.getPlayerLives());
                insertSettings.executeUpdate();
            }
            catch (SQLException sqlException) {
                sqlException.printStackTrace();
                return;
            }

            connection.commit();

        } catch (SQLException | ClassNotFoundException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * Connects to game_data database.
     *
     * @return a connection to the game_data database
     * @throws ClassNotFoundException if the sqlite java data base connection class cannot be located
     * @throws SQLException if a database access errors occur
     */
    private Connection connect() throws ClassNotFoundException, SQLException {
        String DATABASE_NAME = "jdbc:sqlite:game_data.db";
        Class.forName("org.sqlite.JDBC");
        return DriverManager.getConnection(DATABASE_NAME);
    }
}
