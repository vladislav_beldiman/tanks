package org.tanks.GameStates;

import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.DatabaseService.SaveData;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code PauseState} is an extension of the {@code GameState}. It pauses the current run and
 * provides run saving capabilities.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class PauseState extends GameState {
    /**
     * play state saved to be able to continue
     */
    private final PlayState playState;

    /**
     * rectangle used as a button to continue the run
     */
    private final Rectangle continueButton = new Rectangle(498, 180, 300, 70);

    /**
     * rectangle used as a button to start a new run
     */
    private final Rectangle restartButton = new Rectangle(498, 260, 300, 70);

    /**
     * rectangle used as a button to save the run
     */
    private final Rectangle saveButton = new Rectangle(498, 340, 300, 70);

    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle mainMenuButton = new Rectangle(498, 500, 300, 70);

    private int difficulty;

    /**
     * Sole constructor.
     *
     * @param playState play state to save for run continuation
     */
    public PauseState(PlayState playState) {
        this.playState = playState;
    }

    @Override
    public void enter(World world) {
        difficulty = world.getDifficulty();
    }

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (continueButton.contains(clickLocation)) {
                return playState;
            }

            if (restartButton.contains(clickLocation)) {
                return new PlayState(world.getWidth(), world.getHeight(),
                                        world.getDifficulty(), world.getHighscore());
            }

            if (saveButton.contains(clickLocation)) {
                save();
                return null;
            }

            if (mainMenuButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(continueButton.x, continueButton.y,
                            continueButton.width, continueButton.height);
        graphics.fillRect(restartButton.x, restartButton.y,
                            restartButton.width, restartButton.height);
        graphics.fillRect(saveButton.x, saveButton.y,
                            saveButton.width, saveButton.height);
        graphics.fillRect(mainMenuButton.x, mainMenuButton.y,
                            mainMenuButton.width, mainMenuButton.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(continueButton.x, continueButton.y,
                            continueButton.width, continueButton.height);
        graphics.drawRect(restartButton.x, restartButton.y,
                            restartButton.width, restartButton.height);
        graphics.drawRect(saveButton.x, saveButton.y,
                            saveButton.width, saveButton.height);
        graphics.drawRect(mainMenuButton.x, mainMenuButton.y,
                            mainMenuButton.width, mainMenuButton.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Continue", continueButton.x + continueButton.width / 2 - 96,
                                            continueButton.y + continueButton.height - 20);
        graphics.drawString("Restart", restartButton.x + restartButton.width / 2 - 77,
                                            restartButton.y + restartButton.height - 20);
        graphics.drawString("Save", saveButton.x + saveButton.width / 2 - 53,
                                        saveButton.y + saveButton.height - 20);
        graphics.drawString("Menu", mainMenuButton.x + mainMenuButton.width / 2 - 56,
                                        mainMenuButton.y + mainMenuButton.height - 20);
    }

    private void save() {
        SaveData saveData = new SaveData(difficulty, playState.getScore(), playState.getPlayerCoord(),
                                         playState.getPlayerLevel(), playState.getPlayerHp(),
                                         playState.getPlayerLives(), playState.getEnemiesCoord(),
                                         playState.getEnemiesLevel(), playState.getEnemiesHp(),
                                         playState.getObstaclesCoord(), playState.getObstaclesType(),
                                         playState.getPickupsCoord(), playState.getProjectilesCoord(),
                                         playState.getProjectilesDirection(),
                                         playState.getProjectilesType(),
                                         playState.getProjectilesAllegiance(), playState.getTiles());

        Game.LOCATOR.getDatabaseService().updateSaveData(saveData);
    }
}
