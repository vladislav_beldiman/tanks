package org.tanks.GameStates;

import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code StoryState} is an extension of the {@code GameState}. It displays the game story.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class StoryState extends GameState {
    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle continueButton = new Rectangle(498, 500, 300, 70);

    /**
     * rectangle used as a text box for the story text
     */
    private final Rectangle storyTextBox = new Rectangle(5, 150, 1000, 250);

    /**
     * story text
     */
    private static final String story =
            "     In a world of constant war where neither side can gain an upper hand.\n" +
            "You, a lone tankman, find yourself surrounded by bloodthirsty army of the\n" +
            "reds as your allies fail to support you. But, instead of laying down your\n" +
            "arms, you decide to take as many as you can down with you from this\n" +
            "endless stream of enemies.";

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (continueButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(continueButton.x, continueButton.y, continueButton.width, continueButton.height);
        graphics.fillRect(storyTextBox.x, storyTextBox.y, storyTextBox.width, storyTextBox.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(continueButton.x, continueButton.y, continueButton.width, continueButton.height);
        graphics.drawRect(storyTextBox.x, storyTextBox.y, storyTextBox.width, storyTextBox.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Continue", continueButton.x + continueButton.width / 2 - 96, continueButton.y + continueButton.height - 20);

        graphics.setColor(TEXT_COLOR);
        graphics.setFont(TOOLTIP_FONT);
        drawMultilineString(graphics, story, 10, 160);
    }
}
