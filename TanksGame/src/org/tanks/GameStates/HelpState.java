package org.tanks.GameStates;

import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code HelpState} is an extension of the {@code GameState}. It displays the help.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class HelpState extends GameState {
    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle backButton = new Rectangle(498, 580, 300, 70);

    /**
     * rectangle used as a text box for the help text
     */
    private final Rectangle helpTextBox = new Rectangle(5, 50, 843, 350);

    /**
     * help text
     */
    private static final String help = "How to play\n\n" +
            "The goal is to get as high a score you can.\n" +
            "        <- This is your lives counter. When it reaches 0 - you lose.\n" +
            "Press W, S, A, D to move up, down, left and right, respectively.\n" +
            "Press Space to shoot.\n\n" +
            "                    Have fun!";

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (backButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(backButton.x, backButton.y, backButton.width, backButton.height);
        graphics.fillRect(helpTextBox.x, helpTextBox.y, helpTextBox.width, helpTextBox.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(backButton.x, backButton.y, backButton.width, backButton.height);
        graphics.drawRect(helpTextBox.x, helpTextBox.y, helpTextBox.width, helpTextBox.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Back", backButton.x + backButton.width / 2 - 53,
                                        backButton.y + backButton.height - 20);

        graphics.setColor(TEXT_COLOR);
        graphics.setFont(TOOLTIP_FONT);
        drawMultilineString(graphics, help, 10, 60);

        graphics.setColor(LIVES_COLOR);
        graphics.setFont(TEXT_FONT);
        graphics.drawOval(12, 180, 50, 50);
        graphics.drawString(Integer.toString(3), 25, 222);
    }
}
