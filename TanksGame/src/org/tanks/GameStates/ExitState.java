package org.tanks.GameStates;

import org.tanks.World;

/**
 * The {@code ExitState} is an extension of the {@code GameState}. It, simply, terminates the game process on
 * state entry.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class ExitState extends GameState {
    @Override
    public void enter(World world) {
        System.exit(0);
    }
}
