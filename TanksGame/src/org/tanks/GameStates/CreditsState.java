package org.tanks.GameStates;

import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code CreditsState} is an extension of the {@code GameState}. It displays the credits.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class CreditsState extends GameState {
    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle backButton = new Rectangle(498, 580, 300, 70);

    /**
     * rectangle used as a text box for the credits text
     */
    private final Rectangle creditsTextBox = new Rectangle(5, 20, 725, 470);

    /**
     * credits text
     */
    private static final String credits = "Credits\n\n" +
            "Music:\n" +
            "  War Music by Deepak R Gaming (from youtube.com)\n\n" +
            "Sound Effects (from Freesound.org):\n" +
            "  Under Creative Commons 0 license:\n" +
            "    Tank fire Mixed.wav by Cyberkineticfilms\n" +
            "    Repair_metal.wav by zbig77\n\n" +
            "  Under Creative Commons Attribute 3.0 license:\n" +
            "    explosion_012.mp3 by deleted_user_5405837";

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (backButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(backButton.x, backButton.y, backButton.width, backButton.height);
        graphics.fillRect(creditsTextBox.x, creditsTextBox.y,
                            creditsTextBox.width, creditsTextBox.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(backButton.x, backButton.y, backButton.width, backButton.height);
        graphics.drawRect(creditsTextBox.x, creditsTextBox.y,
                            creditsTextBox.width, creditsTextBox.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Back", backButton.x + backButton.width / 2 - 53,
                                        backButton.y + backButton.height - 20);

        graphics.setColor(TEXT_COLOR);
        graphics.setFont(TOOLTIP_FONT);
        drawMultilineString(graphics, credits, 10, 10);
    }
}
