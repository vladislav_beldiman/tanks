package org.tanks.GameStates;

import org.tanks.Assets.Assets;
import org.tanks.Effects.EffectsPool;
import org.tanks.Entities.Components.Input.Direction;
import org.tanks.Entities.Managers.EnemyPool;
import org.tanks.Entities.Managers.ObstacleManager;
import org.tanks.Entities.Managers.PlayerManager;
import org.tanks.Entities.Managers.ProjectileManager;
import org.tanks.Entities.Objects.Obstacles.ObstacleType;
import org.tanks.Entities.Objects.Projectiles.ProjectileType;
import org.tanks.Entities.Allegiance;
import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.Map.Map;
import org.tanks.Map.Tiles.TileType;
import org.tanks.Pickups.PickupsPool;
import org.tanks.DatabaseService.SaveData;
import org.tanks.Score;
import org.tanks.World;

import java.awt.*;
import java.util.Vector;

/**
 * The {@code PlayState} is an extension of the {@code GameState}. It is responsible for all creating,
 * initializing, handling input, updating and drawing all components related to gameplay.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class PlayState extends GameState {
    private final Map map;
    private final ObstacleManager obstacleManager;
    private final PlayerManager playerManager;
    private final EnemyPool enemyPool;
    private final ProjectileManager projectileManager;
    private final PickupsPool pickupsPool;
    private final EffectsPool effectsPool;

    /**
     * current run score
     */
    private final Score score;

    /**
     * cached high score
     */
    private int highscore;

    /**
     * Constructor with no arguments. Used internally by all other constructors to create this' components
     */
    private PlayState() {
        map = new Map();
        obstacleManager = new ObstacleManager();
        playerManager = new PlayerManager();
        enemyPool = new EnemyPool(20, 710);
        projectileManager = new ProjectileManager();
        pickupsPool = new PickupsPool();
        effectsPool = new EffectsPool();

        score = new Score();
    }

    /**
     * Constructor used for a new run.
     * 
     * @param width width of window
     * @param height height of window
     * @param difficulty difficulty
     * @param highscore highscore
     */
    public PlayState(int width, int height, int difficulty, int highscore) {
        this();

        this.highscore = highscore;

        restart(width, height, difficulty);
    }

    /**
     * Constructor used for loading a run.
     *
     * @param width width of window
     * @param height height of window
     * @param highscore highscore
     */
    public PlayState(int width, int height, int highscore) {
        this();

        this.highscore = highscore;

        load();
    }

    /**
     * Restarts all components to default values to initialize for a new run.
     *
     * @param width width of window
     * @param height height of window
     * @param difficulty difficulty
     */
    private void restart(int width, int height, int difficulty) {
        map.restart(width / Assets.TILE_WIDTH, height / Assets.TILE_WIDTH);
        obstacleManager.restart(map,20);
        playerManager.restart();
        enemyPool.restart(7, difficulty + 1);
        projectileManager.restart();
        pickupsPool.restart();
        effectsPool.restart();

        score.resetScore();
    }

    /**
     * Loads saved data and uses it to initialize components for a loaded run.
     */
    private void load() {
        SaveData saveData = Game.LOCATOR.getDatabaseService().getSaveData();

        map.load(saveData.getTiles());
        obstacleManager.load(saveData.getObstacleCoords(), saveData.getObstacleTypes());
        playerManager.load(saveData.getPlayerCoord(), saveData.getPlayerLevel(),
                            saveData.getPlayerHp(), saveData.getPlayerLives());
        enemyPool.load(saveData.getEnemyCoords(), saveData.getEnemyLevels(),
                        saveData.getEnemyHps(), saveData.getDifficulty() + 1);
        projectileManager.load(saveData.getProjectileCoords(), saveData.getProjectileDirections(),
                                saveData.getProjectileTypes(), saveData.getProjectileAllegiances());
        pickupsPool.load(saveData.getPickupCoords());
        effectsPool.restart();

        score.addScore(saveData.getScore());
    }

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        playerManager.handleInput(keyInput);
        enemyPool.handleInput(keyInput);

        if (keyInput.escapePressed()) {
            return new PauseState(this);
        }

        if (playerManager.lost()) {
            return new RunEndState(score.getScore());
        }

        return null;
    }

    @Override
    public void update(World world){
        playerManager.update(world.getBounds(), map, obstacleManager, playerManager, enemyPool,
                             projectileManager, pickupsPool, effectsPool, score);
        enemyPool.update(world.getBounds(), map, obstacleManager, playerManager, enemyPool,
                         projectileManager, pickupsPool, effectsPool, score);
        projectileManager.update(world.getBounds(), map, obstacleManager, playerManager, enemyPool,
                                 projectileManager, pickupsPool, effectsPool, score);
        pickupsPool.update(map, obstacleManager);
        effectsPool.update();
    }

    @Override
    public void draw(Graphics graphics) {
        map.draw(graphics);
        obstacleManager.draw(graphics);
        playerManager.draw(graphics);
        enemyPool.draw(graphics);
        projectileManager.draw(graphics);
        pickupsPool.draw(graphics);
        effectsPool.draw(graphics);

        if (score.getScore() > highscore) {
            graphics.setColor(HIGH_SCORE_COLOR);
        } else {
            graphics.setColor(SCORE_COLOR);
        }

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Score : " + score.getScore(), 70, 50);

        graphics.setColor(LIVES_COLOR);
        graphics.drawOval(12, 5, 50, 50);
        graphics.drawString(Integer.toString(playerManager.getPlayerLives()), 25, 48);
    }

    /**
     * Obtains the map.
     *
     * @return the map
     */
    public Map getMap() { return map; }

    /**
     * Obtains the obstacle manager.
     *
     * @return the obstacle manager
     */
    public ObstacleManager getObstacleManager() { return obstacleManager; }

    /**
     * Obtains the player manager.
     *
     * @return the player manager
     */
    public PlayerManager getPlayerManager() { return playerManager; }

    /**
     * Obtains the enemy pool.
     *
     * @return the enemy pool
     */
    public EnemyPool getEnemyPool() { return enemyPool; }

    /**
     * Obtains the projectile manager.
     *
     * @return the projectile manager
     */
    public ProjectileManager getProjectileManager() { return projectileManager; }

    /**
     * Obtains the pickups pool.
     *
     * @return the pickups pool
     */
    public PickupsPool getPickupsPool() { return pickupsPool; }

    /**
     * Obtains the effects pool.
     *
     * @return the effects pool
     */
    public EffectsPool getEffectsPool() { return effectsPool; }

    /**
     * Obtains the score.
     *
     * @return the score
     */
    public int getScore() { return score.getScore(); }

    /**
     * Obtains the player coordinates.
     *
     * @return a point of the player coordinates
     */
    public Point getPlayerCoord() {
        return playerManager.getPlayerPosition();
    }

    /**
     * Obtains the player level.
     *
     * @return the player level
     */
    public int getPlayerLevel() { return playerManager.getPlayerLevel(); }

    /**
     * Obtains the player health points.
     *
     * @return the player health points
     */
    public int getPlayerHp() {
        return playerManager.getPlayerHp();
    }

    /**
     * Obtains the player lives.
     *
     * @return the player lives
     */
    public int getPlayerLives() {
        return playerManager.getPlayerLives();
    }

    /**
     * Obtains the enemy coordinates.
     *
     * @return a vector of points of the enemy coordinates
     */
    public Vector<Point> getEnemiesCoord() {
        return enemyPool.getEnemiesPosition();
    }

    /**
     * Obtains the enemy levels.
     *
     * @return a vector of enemy levels
     */
    public Vector<Integer> getEnemiesLevel() {
        return enemyPool.getEnemiesLevel();
    }

    /**
     * Obtains the enemy health points.
     *
     * @return a vector of enemy health points
     */
    public Vector<Integer> getEnemiesHp() {
        return enemyPool.getEnemiesHp();
    }

    /**
     * Obtains the obstacle coordinates.
     *
     * @return a vector of points of the obstacle coordinates
     */
    public Vector<Point> getObstaclesCoord() {
        return obstacleManager.getObstaclesPosition();
    }

    /**
     * Obtains the obstacle types.
     *
     * @return a vector of obstacle types
     */
    public Vector<ObstacleType> getObstaclesType() {
        return obstacleManager.getObstaclesType();
    }

    /**
     * Obtains the pickup coordinates.
     *
     * @return a vector of points of pickup coordinates
     */
    public Vector<Point> getPickupsCoord() {
        return pickupsPool.getPickupsPosition();
    }

    /**
     * Obtains the projectile coordinates.
     *
     * @return a vector of points of projectile coordinates
     */
    public Vector<Point> getProjectilesCoord() {
        return projectileManager.getProjectilesPosition();
    }

    /**
     * Obtains the projectile directions.
     *
     * @return a vector of projectile directions
     */
    public Vector<Direction> getProjectilesDirection() {
        return projectileManager.getProjectilesDirection();
    }

    /**
     * Obtains the projectile types.
     *
     * @return a vector of projectile types
     */
    public Vector<ProjectileType> getProjectilesType() {
        return projectileManager.getProjectilesType();
    }

    /**
     * Obtains the projectile allegiances.
     *
     * @return a vector of projectile allegiances
     */
    public Vector<Allegiance> getProjectilesAllegiance() {
        return projectileManager.getProjectilesAllegiance();
    }

    /**
     * Obtains the map tile data.
     *
     * @return an array of map tile data
     */
    public TileType[][] getTiles() {
        return map.getTileData();
    }

    /**
     * Constructor used for demo purpose only.
     *
     * @param width width of window
     * @param height height of window
     * @param difficulty difficulty
     * @param highscore highscore
     * @param demo not used
     */
    public PlayState(int width, int height, int difficulty, int highscore, boolean demo) {
        this();

        this.highscore = highscore;

        demo(width, height, difficulty);
    }

    /**
     * Sets player manager to demo mode and restarts all other components to default values to
     * initialize for a demo run.
     *
     * @param width width of window
     * @param height height of window
     * @param difficulty difficulty
     */
    private void demo(int width, int height, int difficulty) {
        map.restart(width / Assets.TILE_WIDTH, height / Assets.TILE_WIDTH);
        obstacleManager.restart(map,20);
        playerManager.demo();
        enemyPool.restart(7, difficulty + 1);
        projectileManager.restart();
        pickupsPool.restart();
        effectsPool.restart();

        score.resetScore();
    }
}
