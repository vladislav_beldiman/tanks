package org.tanks.GameStates;

import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code SettingsState} is an extension of the {@code GameState}. It allows visualization and change of
 * the game settings. Settings are committed to the database on state exit.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class SettingsState extends GameState {
    /**
     * rectangle used as a button to switch difficulty
     */
    private final Rectangle difficultyButton = new Rectangle(498, 260, 300, 70);

    /**
     * rectangle used as a button to switch sound on and off
     */
    private final Rectangle soundCheckBox = new Rectangle(498, 340, 300, 70);

    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle backButton = new Rectangle(498, 500, 300, 70);

    /**
     * stores the difficulty while in this state for fast access
     */
    private int difficulty;

    /**
     * stores the sound state while in this state for fast access
     */
    private boolean soundOn;

    /**
     * Sole constructor
     *
     * @param difficulty current game difficulty
     */
    public SettingsState(int difficulty) {
        this.difficulty = difficulty;
        soundOn = Game.LOCATOR.getAudioOn();
    }

    @Override
    public void exit(World world) {
        world.setDifficulty(difficulty);
        Game.LOCATOR.getDatabaseService().updateSetting("difficulty", difficulty);
        Game.LOCATOR.getDatabaseService().updateSetting("soundMuted", soundOn ? 0 : 1);
    }

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (difficultyButton.contains(clickLocation)) {
                difficulty = 1 - difficulty;
            }

            if (soundCheckBox.contains(clickLocation)) {
                Game.LOCATOR.switchAudio();
                soundOn ^= true;
            }

            if (backButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(difficultyButton.x, difficultyButton.y, difficultyButton.width, difficultyButton.height);
        graphics.fillRect(soundCheckBox.x, soundCheckBox.y, soundCheckBox.width, soundCheckBox.height);
        graphics.fillRect(backButton.x, backButton.y, backButton.width, backButton.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(difficultyButton.x, difficultyButton.y, difficultyButton.width, difficultyButton.height);
        graphics.drawRect(soundCheckBox.x, soundCheckBox.y, soundCheckBox.width, soundCheckBox.height);
        graphics.drawRect(backButton.x, backButton.y, backButton.width, backButton.height);

        // delimit check box
        graphics.drawLine(soundCheckBox.x + soundCheckBox.width - 70, soundCheckBox.y, soundCheckBox.x + soundCheckBox.width - 70, soundCheckBox.y + soundCheckBox.height);
        // draw tick if sound on
        if (soundOn) {
            graphics.drawLine(soundCheckBox.x + soundCheckBox.width - 70, soundCheckBox.y, soundCheckBox.x + soundCheckBox.width, soundCheckBox.y + soundCheckBox.height);
            graphics.drawLine(soundCheckBox.x + soundCheckBox.width - 70, soundCheckBox.y + soundCheckBox.height, soundCheckBox.x + soundCheckBox.width, soundCheckBox.y);
        }

        graphics.setFont(TEXT_FONT);
        // draw string corresponding to the difficulty
        switch (difficulty) {
            case 0 -> graphics.drawString("Normal", difficultyButton.x + backButton.width / 2 - 70, difficultyButton.y + difficultyButton.height - 20);
            case 1 -> graphics.drawString("Hard", difficultyButton.x + backButton.width / 2 - 53, difficultyButton.y + difficultyButton.height - 20);
        }

        graphics.drawString("Sound", soundCheckBox.x + backButton.width / 2 - 100, soundCheckBox.y + soundCheckBox.height - 20);
        graphics.drawString("Back", backButton.x + backButton.width / 2 - 53, backButton.y + backButton.height - 20);
    }
}
