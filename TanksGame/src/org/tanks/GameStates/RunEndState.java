package org.tanks.GameStates;

import org.tanks.Game;
import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code RunEndState} is an extension of the {@code GameState}. It displays the score achieved in this
 * run, and the high score.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class RunEndState extends GameState {
    /**
     * rectangle used as a button to start another run
     */
    private final Rectangle restartButton = new Rectangle(498, 260, 300, 70);

    /**
     * rectangle used as a button to go back to the main menu
     */
    private final Rectangle mainMenuButton = new Rectangle(498, 420, 300, 70);

    /**
     * run score
     */
    private final int score;

    /**
     * highscore
     */
    private int highscore;

    /**
     * Sole constructor.
     *
     * @param score the score achieved in this run
     */
    public RunEndState(int score) {
        this.score = score;
    }

    @Override
    public void enter(World world) {
        // update the score if a new best was achieved
        if (score > world.getHighscore()) {
            world.setHighscore(score);
            Game.LOCATOR.getDatabaseService().updateSetting("highscore", score);
        }

        highscore = world.getHighscore();
    }

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (restartButton.contains(clickLocation)) {
                return new PlayState(world.getWidth(), world.getHeight(),
                                     world.getDifficulty(), world.getHighscore());
            }

            if (mainMenuButton.contains(clickLocation)) {
                return new MainMenuState();
            }
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(restartButton.x, restartButton.y, restartButton.width, restartButton.height);
        graphics.fillRect(mainMenuButton.x, mainMenuButton.y,
                            mainMenuButton.width, mainMenuButton.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(restartButton.x, restartButton.y, restartButton.width, restartButton.height);
        graphics.drawRect(mainMenuButton.x, mainMenuButton.y,
                            mainMenuButton.width, mainMenuButton.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Restart", restartButton.x + restartButton.width / 2 - 77,
                                            restartButton.y + restartButton.height - 20);
        graphics.drawString("Menu", mainMenuButton.x + mainMenuButton.width / 2 - 56,
                                            mainMenuButton.y + mainMenuButton.height - 20);

        graphics.setColor(HIGH_SCORE_COLOR);
        graphics.drawString("Highscore : " + highscore, 50, 200);

        // if the run score is not a new high score display it in another color
        if (score < highscore) {
            graphics.setColor(SCORE_COLOR);
        }

        graphics.drawString("Score : " + score, 70, 100);
    }
}
