package org.tanks.GameStates;

import org.tanks.Input.MouseInput;
import org.tanks.World;
import org.tanks.Input.KeyInput;

import java.awt.*;

/**
 * The {@code GameState} is an abstract class for all world states.
 *
 * @see World
 * @author Beldiman Vladislav
 */
public abstract class GameState {
    /**
     * color of the background of the button used in multiple states
     */
    protected static final Color BUTTON_BACKGROUND = new Color(223, 216,191);

    /**
     * color of the text used in multiple states
     */
    protected static final Color TEXT_COLOR = Color.black;

    /**
     * font of the text used in multiple states
     */
    protected static final Font TEXT_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 48);

    /**
     * font of the tooltip used in multiple states
     */
    protected static final Font TOOLTIP_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 30);

    /**
     * color of the high score used in multiple states
     */
    protected static final Color HIGH_SCORE_COLOR = new Color(170, 169, 173);

    /**
     * color of the lives hud used in multiple states
     */
    protected static final Color LIVES_COLOR = new Color(108, 108, 108);

    /**
     * color of the score used in multiple states
     */
    protected static final Color SCORE_COLOR = new Color(234, 215, 189);

    /**
     * Method executed on state entry.
     *
     * @param world a world
     */
    public void enter(World world) {}

    /**
     * Method executed on state exit.
     *
     * @param world a world
     */
    public void exit(World world) {}

    /**
     * Handles the input in a state.
     *
     * @param world a world
     * @param keyInput the key input
     * @param mouseInput the mouse input
     * @return a new game state, or null if the game state should not be changed
     */
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        /* do nothing */
        return null;
    }

    /**
     * Updates the state and its components.
     *
     * @param world
     */
    public void update(World world) { /* do nothing */ }

    /**
     * Draws the state components in a graphics context.
     *
     * @param graphics a graphics context
     */
    public void draw(Graphics graphics) { /* do nothing */ }

    /**
     * Draws a multiline string in a graphics context.
     *
     * @param g a graphics context
     * @param text a string
     * @param x x coordinate
     * @param y y coordinate
     */
    void drawMultilineString(Graphics g, String text, int x, int y) {
        int lineHeight = g.getFontMetrics().getHeight();
        for (String line : text.split("\n"))
            g.drawString(line, x, y += lineHeight);
    }
}
