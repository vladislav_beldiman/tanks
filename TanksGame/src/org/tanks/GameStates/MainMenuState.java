package org.tanks.GameStates;

import org.tanks.Input.KeyInput;
import org.tanks.Input.MouseInput;
import org.tanks.World;

import java.awt.*;

/**
 * The {@code MainMenuState} is an extension of the {@code GameState}. It provides access to multiple
 * other states through buttons.
 *
 * @see GameState
 * @author Beldiman Vladislav
 */
public class MainMenuState extends GameState {
    /**
     * rectangle used as a button to start a new run
     */
    private final Rectangle playButton = new Rectangle(498, 100, 300, 70);

    /**
     * rectangle used as a button to load a saved run
     */
    private final Rectangle loadButton = new Rectangle(498, 180, 300, 70);

    /**
     * rectangle used as a button to go to the story menu
     */
    private final Rectangle storyButton = new Rectangle(498, 260, 300, 70);

    /**
     * rectangle used as a button to go to the help menu
     */
    private final Rectangle helpButton = new Rectangle(498, 340, 300, 70);

    /**
     * rectangle used as a button to go to settings
     */
    private final Rectangle settingsButton = new Rectangle(498, 420, 300, 70);

    /**
     * rectangle used as a button to go to credits
     */
    private final Rectangle creditsButton = new Rectangle(498, 500, 300, 70);

    /**
     * rectangle used as a button to exit the game
     */
    private final Rectangle exitButton = new Rectangle(498, 580, 300, 70);

    /**
     * cached highscore
     */
    private int highscore;

    @Override
    public void enter(World world) {
        highscore = world.getHighscore();
    }

    @Override
    public GameState handleInput(World world, KeyInput keyInput, MouseInput mouseInput) {
        if (mouseInput.isMouseClicked()) {
            Point clickLocation = mouseInput.getClickLocation();

            if (playButton.contains(clickLocation)) {
                return new PlayState(world.getWidth(), world.getHeight(),
                                     world.getDifficulty(), world.getHighscore());
            }

            if (loadButton.contains(clickLocation)) {
                return new PlayState(world.getWidth(), world.getHeight(), world.getHighscore());
            }

            if (storyButton.contains(clickLocation)) {
                return new StoryState();
            }

            if (helpButton.contains(clickLocation)) {
                return new HelpState();
            }

            if (settingsButton.contains(clickLocation)) {
                return new SettingsState(world.getDifficulty());
            }

            if (creditsButton.contains(clickLocation)) {
                return new CreditsState();
            }

            if (exitButton.contains(clickLocation)) {
                return new ExitState();
            }
        }

        if (keyInput.demoPressed()) {
            return new PlayState(world.getWidth(), world.getHeight(),
                    world.getDifficulty(), world.getHighscore(), true);
        }

        return null;
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(BUTTON_BACKGROUND);
        graphics.fillRect(playButton.x, playButton.y, playButton.width, playButton.height);
        graphics.fillRect(loadButton.x, loadButton.y, loadButton.width, loadButton.height);
        graphics.fillRect(storyButton.x, storyButton.y, storyButton.width, storyButton.height);
        graphics.fillRect(helpButton.x, helpButton.y, helpButton.width, helpButton.height);
        graphics.fillRect(settingsButton.x, settingsButton.y,
                            settingsButton.width, settingsButton.height);
        graphics.fillRect(creditsButton.x, creditsButton.y,
                            creditsButton.width, creditsButton.height);
        graphics.fillRect(exitButton.x, exitButton.y,
                            exitButton.width, exitButton.height);

        graphics.setColor(TEXT_COLOR);
        graphics.drawRect(playButton.x, playButton.y,
                            playButton.width, playButton.height);
        graphics.drawRect(loadButton.x, loadButton.y,
                            loadButton.width, loadButton.height);
        graphics.drawRect(storyButton.x, storyButton.y,
                            storyButton.width, storyButton.height);
        graphics.drawRect(helpButton.x, helpButton.y,
                            helpButton.width, helpButton.height);
        graphics.drawRect(settingsButton.x, settingsButton.y,
                            settingsButton.width, settingsButton.height);
        graphics.drawRect(creditsButton.x, creditsButton.y,
                            creditsButton.width, creditsButton.height);
        graphics.drawRect(exitButton.x, exitButton.y,
                            exitButton.width, exitButton.height);

        graphics.setFont(TEXT_FONT);
        graphics.drawString("Play", playButton.x + playButton.width / 2 - 47,
                                        playButton.y + playButton.height - 20);
        graphics.drawString("Load", loadButton.x + loadButton.width / 2 - 53,
                                        loadButton.y + loadButton.height - 20);
        graphics.drawString("Story", storyButton.x + storyButton.width / 2 - 55,
                                         storyButton.y + storyButton.height - 20);
        graphics.drawString("Help", helpButton.x + helpButton.width / 2 - 49,
                                        helpButton.y + helpButton.height - 20);
        graphics.drawString("Settings", settingsButton.x + settingsButton.width / 2 - 84,
                                            settingsButton.y + settingsButton.height - 20);
        graphics.drawString("Credits", creditsButton.x + creditsButton.width / 2 - 78,
                                            creditsButton.y + creditsButton.height - 20);
        graphics.drawString("Exit", exitButton.x + exitButton.width / 2 - 42,
                                        exitButton.y + exitButton.height - 20);

        graphics.setColor(HIGH_SCORE_COLOR);
        graphics.drawString("Highscore : " + highscore, 50, 50);
    }
}
